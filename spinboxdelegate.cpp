#include "spinboxdelegate.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QSpinBox>
#include <QtGui/QMessageBox>
#else
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QMessageBox>
#endif
#include <QtGui/QStandardItemModel>
#include <QtWidgets/QApplication>

SpinBoxDelegate::SpinBoxDelegate(QObject *parent) : QItemDelegate(parent)
{
}

QWidget *SpinBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */,
    const QModelIndex &/* index */) const
{
    QSpinBox *editor = new QSpinBox(parent);
    editor->setMinimum(1);
    editor->setMaximum(999999);

    return editor;
}

void SpinBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    int value = index.model()->data(index, Qt::EditRole).toInt();

    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
    spinBox->setValue(value);
}

void SpinBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
    spinBox->interpretText();
    int value = spinBox->value();

    //Validar que la prioridad no está siendo utilizada por otro bloque y si es el caso mostrar el error y mantener el valor actual.
    QStandardItem *blocItem = static_cast<QStandardItemModel*>(model)->itemFromIndex(index.parent());
    for(int i=0, n=blocItem->rowCount(); i<n; i++)
    {
        if(blocItem->child(i)->data(Qt::EditRole).toInt()==value)
        {
            QMessageBox::warning(editor, QApplication::instance()->applicationName(), tr("There is another block with same priority."));
            spinBox->setValue(value);
            return;
        }
    }

    model->setData(index, value, Qt::EditRole);
}

void SpinBoxDelegate::updateEditorGeometry(QWidget *editor,
     const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}
