#ifndef SITEDETAILS_H
#define SITEDETAILS_H

#include <QtSvg/QGraphicsSvgItem>
#include <QtGui/QStandardItem>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QWidget>
#else
#include <QtWidgets/QWidget>
#endif

class Place : public QGraphicsObject
{
    Q_OBJECT

public:
    Place(int x, int y, int e, QGraphicsItem *parent = 0);
    void setNumbVect(QStringList strList);
    QStringList numbVect() const{return coorVect;}
    QStringList *ptrNumbVect(){return &coorVect;}
    QString coordDesc(); //Descripción de la coordenada de la Place
    int getEtat(){return etat;}
    void setEtat(int state);
    void setCurrentColor(QColor color){currentColor = color;}

    QStandardItem *labelItem;
    QStandardItem *blocItem;
    QStandardItem *zoneItem;

    QGraphicsSimpleTextItem *coorItem;
    QGraphicsSimpleTextItem *rowCoorItem;

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget);

    QString transfAtt;//matriz de transformación SVG
    QString id;//SVG'id
    int dbId;
    int bloc; //Prioridad del bloque de asignación automática de plazas. La prioridad es única entre los bloques.
    QString priorite;
    QString contigu;
    QString selectorder;
    QString row;
    QString pos;
    qreal a;
    QWidget *site2dView;

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
    int x, y;
    QColor color;
    QList<QPointF> stuff;
    QRectF _rect;
    QStringList coorVect;
    int etat;//1 - Libre; 2 - Bloquée; 3 - Payée

    QImage picto;
    QMatrix transfMtx;//transformaciones (rotación, traslación, escala) para el elemento
    QRegExp rx;
    QColor currentColor;
};

#endif

