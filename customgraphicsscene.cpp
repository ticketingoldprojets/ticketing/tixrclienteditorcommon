#include "customgraphicsscene.h"
#include "site2dview.h"

CustomGraphicsScene::CustomGraphicsScene(QObject *parent) : QGraphicsScene(parent)
{
    //setItemIndexMethod(QGraphicsScene::NoIndex);
}

void CustomGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Place *place = static_cast<Place*>( itemAt( event->scenePos(), QTransform() ) );

    if(!place || !(place->flags()&QGraphicsItem::ItemIsSelectable))
    {
        QGraphicsScene::mousePressEvent(event);
        return;
    }

    Site2dView *siteView = static_cast<Site2dView*>(site2dView);

    bool m = siteView->getEditionMode();

    if(event->modifiers()&Qt::SHIFT && m)
    {
        if(!siteView->selectedBlocs.contains(place->blocItem->index()))
        {
            QGraphicsScene::mousePressEvent(event);
            return;
        }

        blockSignals(true);

        QList<Place*> placeList = siteView->pathToPlaceMap.value(place->row).values();
        for(int i=placeList.size()-1; i>=0; i--)
        {
            Place *place2 = placeList.at(i);
            if(place!=place2 && place2->blocItem && siteView->selectedBlocs.contains(place2->blocItem->index()))
            {
                place2->setSelected(!place->isSelected());
            }
        }

        blockSignals(false);
    }

    QGraphicsScene::mousePressEvent(event);
}

void CustomGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if((event->buttons()&Qt::LeftButton) && event->modifiers()&Qt::SHIFT)
    {
        Place *place = static_cast<Place*>( itemAt( event->scenePos(), QTransform() ) );

        if(!place || !(place->flags() & QGraphicsItem::ItemIsSelectable))
        {
            QGraphicsScene::mouseMoveEvent(event);
            return;
        }

        if(!place->blocItem || !site2dView->selectedBlocs.contains(place->blocItem->index()))
        {
            QGraphicsScene::mouseMoveEvent(event);
            return;
        }

        blockSignals(true);

        place->setSelected(true);

        QList<Place*> placeList = site2dView->pathToPlaceMap.value(place->row).values();
        for(int i=placeList.size()-1; i>=0; i--)
        {
            Place *place2 = placeList.at(i);
            if(place2->blocItem && site2dView->selectedBlocs.contains(place2->blocItem->index()))
            {
                place2->setSelected(true);
            }
        }
        blockSignals(false);
    }

    QGraphicsScene::mouseMoveEvent(event);
}

void CustomGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Site2dView *siteView = static_cast<Site2dView*>(site2dView);
    bool m = siteView->getEditionMode();

    if(!m)
    {
        QList<QGraphicsItem *> selItems = selectedItems();
        QList<Place*> placeForLock;
        for(int i=0, n=selItems.size(); i<n; i++)
        {
            Place *place = static_cast<Place*>( selItems.at(i) );

            if(place->getEtat()==1 && (place->flags() & QGraphicsItem::ItemIsSelectable))
            {
                placeForLock << place;
            }
        }

#ifndef TIXREDITOR
        if(!placeForLock.isEmpty())
            siteView->setPlaceState(placeForLock, 2);
#endif
    }

    QGraphicsScene::mouseReleaseEvent(event);
}
