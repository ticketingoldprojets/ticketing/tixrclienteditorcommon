#ifndef SERIEDIALOG_H
#define SERIEDIALOG_H

#include <QtGlobal>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QDialog>
#else
#include <QtWidgets/QDialog>
#endif

namespace Ui {
class SerieDialog;
}

class SerieDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SerieDialog(QWidget *parent = 0);
    ~SerieDialog();
    
private:
    Ui::SerieDialog *ui;
};

#endif // SERIEDIALOG_H

