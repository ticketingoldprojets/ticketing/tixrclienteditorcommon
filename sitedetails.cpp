#include "sitedetails.h"
#include "site2dview.h"
#include <QtSvg/QSvgRenderer>
#include <QtGui/QPainter>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QGraphicsSceneMouseEvent>
#else
#include <QtWidgets/QGraphicsSceneMouseEvent>
#endif
#include <QtCore/QFile>

Place::Place(int x, int y, int e, QGraphicsItem *parent) : QGraphicsObject(parent)
{
    this->x = x;
    this->y = y;
    setZValue(0.5);

    setFlags(QGraphicsItem::ItemIsSelectable);

    coorItem = 0;
    rowCoorItem = 0;
    blocItem = 0;
    bloc = 0;//Significa que el asiento no pertenece a ningún bloque
    labelItem = 0;
    zoneItem = 0;

    _rect = QRectF(0, 0, 16, 16);

    site2dView = 0;
    etat = e;

    rx = QRegExp("fill:#[A-Fa-f0-9]{6}");

    //setCursor(etat==1?Qt::PointingHandCursor:Qt::ArrowCursor);
    setCursor(Qt::PointingHandCursor);
}

void Place::setNumbVect(QStringList strList)
{
    coorVect = strList;
    if(!coorVect.isEmpty())
    {
        coorItem->setText(coorVect.last());
        coorItem->show();
    }

    if(coorVect.size()>1 && !pos.toInt())
    {
        rowCoorItem->setText(coorVect.at(coorVect.size()-2));
        rowCoorItem->show();
    }
}

QString Place::coordDesc()
{
    return "";//TODO: tomar el schema de numeración y escribir el label del nivel junto al valor, Ex. "Bloc: Pair; Rang: A; Sičge: 45"
}

QRectF Place::boundingRect() const
{
    return _rect;
}

QPainterPath Place::shape() const
{
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

void Place::paint(QPainter *painter, const QStyleOptionGraphicsItem * /*option*/, QWidget * /*widget*/)
{
    Site2dView *siteView = static_cast<Site2dView*>(site2dView);

    bool m = siteView->getEditionMode();

    /*Representar el estado del asiento: libre, bloqueado, vendido,...
    * Incluso en el modo edición se visualiza el estado de la plaza siempre que la info. esté presente en el plano de la sala.
    * Esto puede ser de utilidad a la hora de editar una seance que ya está siendo vendida, para saber que asientos pueden ser eliminados por ejemplo.
    */
    if(m)
    {
        //Modo edición
        switch(siteView->categInfoToShow)
        {
            case 1://zones de tarifs
            {
                rowCoorItem->hide();
                coorItem->hide();

                QColor zColor = Qt::black;
                if(zoneItem && siteView->selectedZones.contains(zoneItem->index()))
                {
                    zColor = zoneItem->data(Qt::DecorationRole).value<QColor>();
                }

                currentColor = zColor;

                break;
            }

            case 2://Blocs de remplissage automatique
            {
                coorItem->setText(selectorder);
                coorItem->show();

                QColor bColor = blocItem->data(Qt::DecorationRole).value<QColor>();

                if(!siteView->selectedBlocs.contains(blocItem->index()))
                    bColor = Qt::black;

                currentColor = bColor;

                if(siteView->selectedBlocs.size()==1 && siteView->rowToFirstPlaceInBlocMap.contains(row))//Mostrar el orden de la fila en el primer item de la fila que pertenece al bloque seleccionado
                {
                    rowCoorItem->setText(pos);
                    rowCoorItem->show();
                }
                else
                    rowCoorItem->hide();

                break;
            }

            case 3:;//numérotation
                int s =  coorVect.size();

                if(s)
                {
                    coorItem->setText(coorVect.last());
                }

                if(s>1)
                {
                    rowCoorItem->setText(coorVect.at(s-2));
                }

                coorItem->setVisible(s);
                rowCoorItem->setVisible(s>1);

                currentColor = Qt::black;
        }
    }
    else
    {
        //Modo venta (por defecto, se visualizan todas las zonas de tarifa y la numeración de todos los asientos)
        QColor zColor = zoneItem ? zoneItem->data(Qt::DecorationRole).value<QColor>() : Qt::black;

        currentColor = zColor;

        //numeración
        int s =  coorVect.size();

        if(s)
        {
            coorItem->setText(coorVect.last());
        }

        if(s>1)
        {
            rowCoorItem->setText(coorVect.at(s-2));
        }

        coorItem->setVisible(s);
        rowCoorItem->setVisible(s>1);
    }

    QImage *imgPtr = siteView->getCachedImg(etat, currentColor.name(), isSelected());
    Q_ASSERT(imgPtr);
    painter->drawImage(boundingRect(), *imgPtr);
}

void Place::setEtat(int state)
{
    if(etat != state)
    {
        etat = state;
        update();
    }
}

QVariant Place::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if(change == ItemSelectedChange)
    {
        Site2dView *siteView = static_cast<Site2dView*>(site2dView);
        if(blocItem && site2dView && siteView->selectedBlocs.size()==1 && siteView->selectedBlocs.contains(blocItem->index()))
        {
            if(value.toBool())
            {
                if(siteView->selectedRowsMap.contains(row))
                {
                    int v = siteView->selectedRowsMap.value(row);
                    siteView->selectedRowsMap.insert(row, v++);
                }
                else
                {
                    siteView->selectedRowsMap.insert(row, 1);
                    siteView->selectedRows.append(row);
                }
            }
            else
            {
                if(siteView->selectedRowsMap.contains(row))
                {
                    int v = siteView->selectedRowsMap.value(row);
                    if(--v)
                        siteView->selectedRowsMap.insert(row, v);
                    else
                    {
                        siteView->selectedRowsMap.remove(row);
                        siteView->selectedRows.removeOne(row);
                    }
                }
            }
        }
    }

    return QGraphicsItem::itemChange(change, value);
}
//-----------------------------------------------------------------

