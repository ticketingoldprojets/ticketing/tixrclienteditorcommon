#include "site2dview.h"
#include "schemanumdialog.h"
#include "../TixRealmClientPlugins/RotationWidgetPlugin/rotationwidget.h"
#include "zonedialog.h"
#include "reservoirdialog.h"
#include "../TixRealmClient/src/core/datarole.h"
#include "../TixRealmClient/src/core/datacolumn.h"
#include "zoneheaderview.h"
#include "../TixRealmClient/lib/cJSON/cJSON.h"

#ifndef QT_NO_OPENGL
#include <QtOpenGL/QtOpenGL>
#endif

#include <QtCore/qmath.h>
#include <QtGui/QTouchEvent>
#include <QtGui/QStandardItem>

#include <QtUiTools/QUiLoader>

#include <QtXml/QDomDocument>
#include <QtSvg/QSvgRenderer>

#define ZOOM_UNIT 15 //intervalo de zoom tras el cual ocurre una actualización en el renderizado de las imágenes en la caché

#ifdef TIXREDITOR
Site2dView::Site2dView(QProgressBar *pBar, bool editorMode, bool inEditor, QWidget *parent) :
    QWidget(parent), progBar(pBar), editionMode(editorMode), isInEditor(inEditor)
#else
Site2dView::Site2dView(QProgressBar *pBar, bool editorMode, bool inEditor, TixRealmCore *mObj, QWidget *parent) :
    TixrWidget("site2dview", mObj, "pb1btn.png", parent), progBar(pBar), editionMode(editorMode), isInEditor(inEditor)
#endif
{
#ifdef TIXREDITOR
    QUiLoader loader;
    QFile f(":/formconfig/site2dview.ui");
    f.open(QFile::ReadOnly);
    QWidget *dynWidget =  loader.load(&f, this);
    f.close();

    QString wName = dynWidget->objectName();
    dynWidget->setObjectName("dynWidget");
    setObjectName(wName);

    Q_ASSERT(dynWidget);

    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->addWidget(dynWidget);

    setWindowTitle(dynWidget->windowTitle());
    dynWidget->setWindowTitle("");
#else
    setStateIconsCache(tixrCore->stateIconsCache);
#endif

    this->tixrWidgetType = TIXR_PLAN;
    tixrGraphicsView = static_cast<TixrGraphicsView*>(findChild<QGraphicsView*>("tixrGraphicsView"));
    ctlPannel = findChild<QWidget*>("ctlPannel");
    auxPannel = findChild<QWidget*>("auxPannel");

    tableView = findChild<QTableView*>("tableView");
    tableView_2 = findChild<QTableView*>("tableView_2");
    tableView_3 = findChild<QTableView*>("tableView_3");

    //Gestión de la visualización de las zonas de edición: ocultar las no activas
    QWidget *widget = findChild<QWidget*>("widget_9");//bloques de asignación automática de asientos
    if(widget)
        widget->hide();
    widget = findChild<QWidget*>("widget_10");//Edición de la numeración
    if(widget)
        widget->hide();
    widget = findChild<QWidget*>("widget_11");//Réservoirs
    if(widget)
        widget->hide();

    actualRotationAngle = 0;
    totalAngle = 0;
    totalScaleFactor = 1;

    toSave = false;
#ifndef QT_NO_FILESYSTEMWATCHER
    fileWatcher = 0;
#endif

    statesLimit = 5;//Libre, Bloquée, Occupée, Payée, Indisponible

    //rx = QRegExp("fill:#[A-Fa-f0-9]{6}");//para la modificación del color en las imágenes SVG

    fondItem = 0;
    blocItem = 0;

    zonesStdModel = new QStandardItemModel(this);
    zonesItem = new QStandardItem;
    zonesStdModel->appendRow(zonesItem);
    QStringList headerLabels;
    headerLabels << tr("Capacity");
    headerLabels << tr("Seating area");
    headerLabels << tr("Unique identifier");
    headerLabels << tr("Price");
    zonesStdModel->setHorizontalHeaderLabels(headerLabels);

    schemaStdModel = new QStandardItemModel(this);
    schemaItem = new QStandardItem;
    schemaStdModel->appendRow(schemaItem);
    numItemsStdModel = new QStandardItemModel(this);
    numItem = new QStandardItem;
    numItemsStdModel->appendRow(numItem);

    blocsStdModel = new QStandardItemModel(this);
    blocItem = new QStandardItem;
    blocsStdModel->appendRow(blocItem);
    headerLabels.clear();
    headerLabels << tr("Name");
    headerLabels << tr("Capacity");
    headerLabels << tr("Fill first row in normal order");
    blocsStdModel->setHorizontalHeaderLabels(headerLabels);

    blocsDelegate = new SpinBoxDelegate(this);
    tableView_3->setItemDelegate(blocsDelegate);

    ZoneHeaderView *myHView = new ZoneHeaderView(Qt::Vertical, tableView);
    tableView->setVerticalHeader(myHView);
    myHView->setSite2dView(this);
    myHView->show();

    siteCtlsContainer = 0;
    ctlWidget = 0;
    mapNavigationWindow = 0;

    if(progBar)
        progBar->setRange(1, 100);

    colorList.append("#000000");//el negro es el color por defecto
}

Site2dView::~Site2dView()
{
    if(siteCtlsContainer)
    {
        delete siteCtlsContainer;
    }

    for(int i=0, n=zonesItem->rowCount(); i<n; i++)
        delete zonesItem->child(i)->data(DataRole::placesinset()).value<QList<Place*>*>();
}

QList< QPair<QString,QString> > Site2dView::getZonesToGrilleMap()
{
    QList< QPair<QString,QString> > result;
    for(int i=0, n=zonesItem->rowCount(); i<n; i++)
    {
        QStandardItem *hItem = zonesStdModel->verticalHeaderItem(i);
        QVariant v = hItem->data(DataRole::ztarnom());
        if(v.isValid())
        {
            result << QPair<QString,QString>(hItem->text(), v.toString());
        }
    }
    return result;
}

void Site2dView::loadPlan(QIODevice *iod)
{
    ioDev = iod;

    const QFile *f = 0;
    QString cn = ioDev->metaObject()->className();
    bool fromFile = false;
    if(cn == "QFile")
    {
        f = static_cast<const QFile*>(ioDev);
        fromFile = true;
    }

    if(f)
    {
        if(!f->exists())
        {
            eMsg = tr("File not found!");
            if(progBar)
                progBar->setValue(100);
            return;
        }

        QFileInfo fi(f->fileName());
        setWindowTitle(fi.baseName());
        svgFile = f->fileName();
    }

    resetButton = findChild<QToolButton*>("resetButton");
    zoomSlider = findChild<QSlider*>("zoomSlider");
    rotateSlider = findChild<QSlider*>("rotateSlider");
    tbAddNumLevel = findChild<QToolButton*>("tbAddNumLevel");
    tbDelNumLevel = findChild<QToolButton*>("tbDelNumLevel");
    gBoxZone = findChild<QGroupBox*>("gBoxZone");
    gBoxBloc = findChild<QGroupBox*>("gBoxBloc");
    gBoxNum = findChild<QGroupBox*>("gBoxNum");
    gBoxReservoir = findChild<QGroupBox*>("gBoxReservoir");
    columnView = findChild<QColumnView*>("columnView");

    connect(zoomSlider, SIGNAL(valueChanged(int)), this, SLOT(setupMatrix()), Qt::UniqueConnection);
    connect(rotateSlider, SIGNAL(valueChanged(int)), this, SLOT(setupMatrix()), Qt::UniqueConnection);
    connect(tixrGraphicsView->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(setResetButtonEnabled()));
    connect(tixrGraphicsView->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(setResetButtonEnabled()));
    connect(tableView_2, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editSchemaElem(QModelIndex)));
    connect(gBoxZone, SIGNAL(clicked(bool)), this, SLOT(changeGroupBoxState(bool)));
    connect(gBoxBloc, SIGNAL(clicked(bool)), this, SLOT(changeGroupBoxState(bool)));
    connect(gBoxNum, SIGNAL(clicked(bool)), this, SLOT(changeGroupBoxState(bool)));
    connect(gBoxReservoir, SIGNAL(clicked(bool)), this, SLOT(changeGroupBoxState(bool)));
    connect(tableView_2->horizontalHeader(), SIGNAL(sectionResized(int,int,int)), this, SLOT(resizeColumnView(int,int,int)));

    if(fromFile)
        populateScene();
#ifndef TIXREDITOR
    else
        populateSceneFromJson();
#endif

    if(!eMsg.isEmpty())
        return;

    //--------------------------Calcular los límites de zoom----------------------
    tixrGraphicsView->setAttribute(Qt::WA_DontShowOnScreen);
    tixrGraphicsView->show();//forzar el layout para tener las dimensiones finales del widget

    QSizeF tixrGraphicsViewSize = tixrGraphicsView->size();

    //int w1 = 3. * tixrGraphicsViewSize.width() / 4.;
    //int w2 = tixrGraphicsViewSize.width() - w1;

    //mínimo
    QSizeF sceneSize = tixrGraphicsView->scene()->sceneRect().size();
    qreal w = sceneSize.width();
    sceneSize.scale(tixrGraphicsViewSize, Qt::KeepAspectRatio);
    qreal minScale = sceneSize.width() / w;

    qreal scaleIdx = 50 * qLn(minScale) / qLn(2.);// + 250;

    zoomSlider->setMinimum(scaleIdx);

    //máximo
    qreal maxScale = tixrGraphicsViewSize.height() / 160.;//la escala máxima es la que permite a un asiento llegar a ser 1/10 del alto del graphicsView. La dimensión original del icono es 16x16.
    scaleIdx = 50 * qLn(maxScale) / qLn(2.);// + 250;
    zoomSlider->setMaximum(scaleIdx);

    zoomSlider->setValue(zoomSlider->minimum());
    //-------------------------------------------------------------------------------

    setupMatrix();

#ifndef TIXREDITOR
    connect(zonesStdModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(prixItemChecked(QStandardItem*)));
#endif
    tableView->setModel(zonesStdModel);
    tableView->setRootIndex(zonesItem->index());
    tableView_2->setModel(schemaStdModel);
    tableView_2->setRootIndex(schemaItem->index());
    columnView->setModel(numItemsStdModel);
    columnView->setRootIndex(numItem->index());
    tableView_3->setModel(blocsStdModel);
    tableView_3->setRootIndex(blocItem->index()) ;

    QList<int> list;
    QHeaderView *hView = tableView_2->horizontalHeader();
    for(int i=0, n=hView->count(); i<n; i++)
        list << hView->sectionSize(i);
    columnView->setColumnWidths(list);

    QToolButton *tbSellingMode = ctlPannel->findChild<QToolButton*>("tbSellingMode");
    if(isInEditor)
        tbSellingMode->hide();
    else
        connect(tbSellingMode, SIGNAL(clicked()), this, SLOT(onTbSellingModeClicked()), Qt::QueuedConnection);

    QWidget *mapNavW = ctlPannel->findChild<QWidget*>("mapNavigationWindow");
    if(mapNavW)
    {
        mapNavigationWindow = static_cast<MapNavigationWindow*>(mapNavW);
        mapNavigationWindow->setMinimumZoomIndex(zoomSlider->minimum());

        connect(this, SIGNAL(sceneRotated(int)), mapNavigationWindow, SLOT(rotate(int)));
        connect(zoomSlider, SIGNAL(valueChanged(int)), mapNavigationWindow, SLOT(resizeOverViewFrame(int)));

        mapNavigationWindow->setGraphicsView(tixrGraphicsView);
    }

    if(editionMode)
    {
#ifndef QT_NO_FILESYSTEMWATCHER
        fileWatcher = new QFileSystemWatcher(this);
        fileWatcher->addPath(svgFile);
        connect(fileWatcher, SIGNAL(fileChanged(QString)), this, SLOT(changeFile()));
#endif
    }
    else
    {
        siteCtlsContainer = new QWidget;

        QUiLoader loader;
        QFile f(":/formconfig/planCtlWidget.ui");
        Q_ASSERT(f.open(QFile::ReadOnly));
        ctlWidget =  loader.load(&f);
        f.close();

        QWidget *rotWidget = ctlWidget->findChild<QWidget*>("rotationWidget");
        Q_ASSERT(rotWidget);
        connect(static_cast<RotationWidget*>(rotWidget), SIGNAL(valueChanged(int)), rotateSlider, SLOT(setValue(int)), Qt::QueuedConnection);

        QSlider *ctlWidgetSlider = ctlWidget->findChild<QSlider*>("horizontalSlider");
        ctlWidgetSlider->setMinimum(zoomSlider->minimum());
        ctlWidgetSlider->setMaximum(zoomSlider->maximum());
        ctlWidgetSlider->setValue(zoomSlider->value());
        connect(ctlWidgetSlider, SIGNAL(valueChanged(int)), zoomSlider, SLOT(setValue(int)));

        QPushButton *pbEditionMode = ctlWidget->findChild<QPushButton*>("pbEditionMode");
        connect(pbEditionMode, SIGNAL(clicked()), this, SLOT(onPbEditionModeClicked()), Qt::QueuedConnection);

        QWidget *mapNavW = ctlWidget->findChild<QWidget*>("mapNavigationWindow");
        if(mapNavW)
        {
            mapNavigationWindow = static_cast<MapNavigationWindow*>(mapNavW);
            int mz = zoomSlider->minimum();
            mapNavigationWindow->setMinimumZoomIndex(mz);

            connect(this, SIGNAL(sceneRotated(int)), mapNavigationWindow, SLOT(rotate(int)));
            connect(zoomSlider, SIGNAL(valueChanged(int)), mapNavigationWindow, SLOT(resizeOverViewFrame(int)));

            mapNavigationWindow->setGraphicsView(tixrGraphicsView);
        }

#ifndef TIXREDITOR
        QPushButton *pbUnlock = ctlWidget->findChild<QPushButton*>("pbUnlock");
        connect(pbUnlock, SIGNAL(clicked()), this, SLOT(unlockPlaces()));
#endif

        ctlWidget->setParent(siteCtlsContainer);
        ctlWidget->setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
        ctlWidget->setAttribute(Qt::WA_TranslucentBackground, true);
        ctlWidget->setAttribute(Qt::WA_DeleteOnClose, true);

        ctlW1 = ctlWidget->findChild<QWidget*>("ctlW1");
        ctlW1->installEventFilter(this);

        ctlW2 = ctlWidget->findChild<QWidget*>("ctlW2");
        ctlW2->installEventFilter(this);

        ctlW3 = ctlWidget->findChild<QWidget*>("ctlW3");
        ctlW3->installEventFilter(this);

#ifndef TIXREDITOR
        spCtlW = ctlWidget->findChild<QSpinBox*>("spCtlW");
#endif

        ctlWidget->move(mapToGlobal(QPoint(0,0)));
        ctlWidget->show();

        auxPannel->hide();
        ctlPannel->hide();//Panel del modo edición
    }

    tixrGraphicsView->setAttribute(Qt::WA_DontShowOnScreen, false);

    QMetaObject::connectSlotsByName(this);
}

/**
  * Sets the current centerpoint.  Also updates the scene's center point.
  * Unlike centerOn, which has no way of getting the floating point center
  * back, SetCenter() stores the center point.  It also handles the special
  * sidebar case.  This function will claim the centerPoint to sceneRec ie.
  * the centerPoint must be within the sceneRec.
  */
//Set the current centerpoint in the
void Site2dView::setCenterOfView(const QPointF& centerPoint) {
    //Get the rectangle of the visible area in scene coords
    QRectF visibleArea = tixrGraphicsView->mapToScene(rect()).boundingRect();

    //Get the scene area
    QRectF sceneBounds = tixrGraphicsView->sceneRect();

    double boundX = visibleArea.width() / 2.0;
    double boundY = visibleArea.height() / 2.0;
    double boundWidth = sceneBounds.width() - 2.0 * boundX;
    double boundHeight = sceneBounds.height() - 2.0 * boundY;

    //The max boundary that the centerPoint can be to
    QRectF bounds(boundX, boundY, boundWidth, boundHeight);

    QPointF CurrentCenterPoint;
    if(bounds.contains(centerPoint)) {
        //We are within the bounds
        CurrentCenterPoint = centerPoint;
    } else {
        //We need to clamp or use the center of the screen
        if(visibleArea.contains(sceneBounds)) {
            //Use the center of scene ie. we can see the whole scene
            CurrentCenterPoint = sceneBounds.center();
        } else {

            CurrentCenterPoint = centerPoint;

            //We need to clamp the center. The centerPoint is too large
            if(centerPoint.x() > bounds.x() + bounds.width()) {
                CurrentCenterPoint.setX(bounds.x() + bounds.width());
            } else if(centerPoint.x() < bounds.x()) {
                CurrentCenterPoint.setX(bounds.x());
            }

            if(centerPoint.y() > bounds.y() + bounds.height()) {
                CurrentCenterPoint.setY(bounds.y() + bounds.height());
            } else if(centerPoint.y() < bounds.y()) {
                CurrentCenterPoint.setY(bounds.y());
            }

        }
    }

    //Update the scrollbars
    tixrGraphicsView->centerOn(CurrentCenterPoint);
}

#ifndef TIXREDITOR
#define stickyMargin 30 //margen para la atracción de las ventanas flotantes

bool Site2dView::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        if(obj == ctlW1 || obj == ctlW2 || obj == ctlW3)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->button() == Qt::LeftButton)
            {
                dragPosition = me->globalPos() - ctlWidget->pos();
                me->accept();
                return true;
            }
        }
    }else

    if(event->type() == QEvent::MouseMove)
    {
        if(obj == ctlW1 || obj == ctlW2 || obj == ctlW3)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->buttons() & Qt::LeftButton)
            {
                QPoint p = me->globalPos() - dragPosition;
                QRect r(mapToGlobal(QPoint(0,0)), size());

                if(p.x() - stickyMargin < r.left())
                    p.setX(r.left());

                if(p.x() + stickyMargin + ctlWidget->size().width() > r.right())
                    p.setX(r.right() - ctlWidget->size().width());

                if(p.y() - stickyMargin < r.top())
                    p.setY(r.top());

                if(p.y() + stickyMargin + ctlWidget->size().height() > r.bottom())
                    p.setY(r.bottom() - ctlWidget->size().height());

                ctlWidget->move(p);

                me->accept();
                return true;
            }
        }else
        {
            QString cn = obj->metaObject()->className();
            if(cn == "QListView")
            {
                QListView *lv = static_cast<QListView*>(obj);
                int n = lv->property("tixrNSeat").toInt();
                spCtlW->setMaximum(n);
            }
        }

    }

    return false;
}
#endif

void Site2dView::setOnTopWidgetsVisibility(bool visible)
{
    if(ctlWidget && (ctlWidget->isVisible() != visible) && !editionMode)
    {
        ctlWidget->setVisible(visible);
    }
}

#ifndef TIXREDITOR
void Site2dView::hideEvent(QHideEvent * event)
{
    if(ctlWidget)
        ctlWidget->hide();
    TixrWidget::hideEvent(event);
}

void Site2dView::showEvent(QShowEvent * event)
{
    if(ctlWidget &&  !editionMode)
        ctlWidget->show();
    TixrWidget::showEvent(event);
}
#endif

void Site2dView::setEditionMode(bool mode)
{
    editionMode = mode || isInEditor;

    //adaptar la interfaz al modo de funcionamiento actual.
    if(ctlWidget)
    {
        ctlWidget->setVisible(!editionMode);
    }

    if(ctlPannel)
        ctlPannel->setVisible(editionMode);

    if(auxPannel)
        auxPannel->setVisible(editionMode);
}

void Site2dView::onPbEditionModeClicked()
{
    setEditionMode(true);
}

void Site2dView::onTbSellingModeClicked()
{
    setEditionMode(false);
}

const qreal Pi = 3.141592654f;
inline qreal RadToDeg(qreal a)
{
    return a * 180. / Pi;
}

void Site2dView::populateScene()
{
    CustomGraphicsScene *scene = new CustomGraphicsScene;
    scene->setSite2dView(this);

    QMap<QString,QStandardItem*> zonesMap;

    ioDev->open(QIODevice::ReadOnly);
    QXmlStreamReader xml(ioDev->readAll().constData());
    ioDev->close();

    int rowIndex = 0, docWidth, docHeight;
    QRegExp rx("matrix\\(([\\d.\\-eE]+),([\\d.\\-eE]+),[\\d.\\-eE]+,[\\d.\\-eE]+,([\\d.\\-eE]+),([\\d.\\-eE]+)\\)");
    if(xml.readNextStartElement() && xml.name() == "svg")
    {
        docWidth = xml.attributes().value("width").toString().toInt();
        docHeight = xml.attributes().value("height").toString().toInt();
        if(!(docWidth && docHeight))
        {
            eMsg = tr("Les dimensions du fichier ne sont pas valides");
            delete scene;
            return;
        }

        while(!xml.atEnd())
        {
            xml.readNext();

            if(xml.name()=="metadata" && xml.isStartElement())
            {
                while(!xml.atEnd())
                {
                    xml.readNext();
                    if(xml.name()=="TixRealm" && xml.isStartElement())
                    {
                        while(!xml.atEnd())
                        {
                            xml.readNext();
                            if(xml.name()=="Zones" && xml.isStartElement())
                            {
                                int endElemCounter = 1;
                                while(!xml.atEnd())
                                {
                                    xml.readNext();
                                    if(xml.name()=="Zone" && xml.isStartElement())
                                    {
                                        endElemCounter = 0;
                                        QList<QStandardItem*> rowItems;

                                        QString nom = xml.attributes().value("nom").toString();
                                        QString cplaces = xml.attributes().value("cplaces").toString();
                                        QString couleur = xml.attributes().value("couleur").toString();
                                        bool isInt;
                                        bool tZone = xml.attributes().value("tZone").toInt(&isInt);
                                        QString idUnique = xml.attributes().value("idUnique").toString();

                                        if(nom.isEmpty() || cplaces.isEmpty() || couleur.isEmpty() || !isInt)
                                        {
                                           eMsg = tr("Invalid file format: wrong tariff zones definition");
                                           delete scene;
                                           return;
                                        }

                                        QStandardItem *cplacesItem = new QStandardItem;
                                        cplacesItem->setData(QColor(couleur), Qt::DecorationRole);
                                        cplacesItem->setData(QVariant::fromValue(new QList<Place*>), DataRole::placesinset());
                                        cplacesItem->setData(QVariant(tZone?0:cplaces.toInt()), Qt::DisplayRole);//Si la zona es Placée pongo la cantidad de plazas a cero y más adelante cuento la cantidad real de plazas que pertenecen a la zona
                                        cplacesItem->setData(nom, DataRole::ztarnom());

                                        //actualizar la lista de colores únicos
                                        if(!colorList.contains(couleur))
                                        {
                                            colorList.append(couleur.toLower());
                                        }

                                        rowItems << cplacesItem;
                                        rowItems << new QStandardItem(tZone?tr("Yes"):tr("No"));//TRUE = placée
                                        rowItems << new QStandardItem(idUnique);

                                        QStandardItem *prixColumnItem = new QStandardItem;
                                        prixColumnItem->setCheckable(true);
                                        rowItems << prixColumnItem;

                                        zonesItem->appendRow(rowItems);

                                        zonesStdModel->setVerticalHeaderItem(zonesItem->rowCount()-1, new QStandardItem(nom));
                                        zonesMap.insert(nom, cplacesItem);
                                    }else
                                        if(xml.isEndElement())
                                            endElemCounter++;

                                    if(endElemCounter==2)
                                        break;
                                }
                            }

                            if(xml.name()=="SchemaNum" && xml.isStartElement())
                            {
                                QStringList headerLabels;

                                int endElemCounter = 1;
                                while(!xml.atEnd())
                                {
                                    xml.readNext();
                                    if(xml.name()=="SchemaElem" && xml.isStartElement())
                                    {
                                        endElemCounter = 0;
                                        QString nom = xml.attributes().value("nom").toString();
                                        QString serie = xml.attributes().value("serie").toString();
                                        int level = xml.attributes().value("level").toString().toInt();

                                        if(nom.isEmpty())
                                        {
                                           eMsg = tr("Invalid file format: bad numbering schema.");
                                           delete scene;
                                           return;
                                        }

                                        SerieManager sm(serie);
                                        if(!sm.isValid())
                                        {
                                            eMsg = tr("Invalid file format: bad numbering schema.");
                                            delete scene;
                                            return;
                                        }

                                        QString desc = QString("Name: %1\nSerie: %2\nSize of serie: %3");
                                        int N = sm.getSize();
                                        desc = desc.arg(nom, serie, N>-1?QString::number(N):"INF.");

                                        QStandardItem *schemaElemItem = new QStandardItem(desc);
                                        schemaElemItem->setData(nom, DataRole::nomsitenumbitem());
                                        schemaElemItem->setData(serie, DataRole::seriesitenumbitem());
                                        schemaElemItem->setData(level, DataRole::levelsitenumbitem());

                                        int i=0, n=schemaItem->columnCount();
                                        for(; i<n; i++)
                                        {
                                            int l2 = schemaItem->child(0,i)->data(DataRole::levelsitenumbitem()).toInt();
                                            if(level<l2)
                                            {
                                                headerLabels.insert(i, nom);
                                                schemaItem->insertColumn(i, QList<QStandardItem*>()<<schemaElemItem);
                                                break;
                                            }
                                            else
                                                if(level==l2)
                                                {
                                                   eMsg = tr("Invalid file format: bad numbering schema. <level> value should be unique.");
                                                   delete scene;
                                                   return;
                                                }
                                        }
                                        if(i==n)
                                        {
                                            headerLabels.append(nom);
                                            schemaItem->appendColumn(QList<QStandardItem*>()<<schemaElemItem);
                                        }
                                    }else
                                        if(xml.isEndElement())
                                            endElemCounter++;

                                    if(endElemCounter==2)
                                        break;
                                }
                                schemaStdModel->setHorizontalHeaderLabels(headerLabels);
                                break; //ELIMINAR SI SE VA A LEER OTRAS DEFINICIONES TixRealm
                            }
                        }
                        break;
                    }
                }
            }else

            if(xml.name()=="g" && xml.isStartElement() && xml.attributes().value("inkscape:label").toString()=="Places")
            {
                if(progBar)
                    progBar->setValue(10);
                int endElemCounter = 1;
                while(!xml.atEnd())
                {
                    xml.readNext();
                    if(xml.name()=="use" && xml.isStartElement())
                    {
                        endElemCounter = 0;
                        QString pId = xml.attributes().value("id").toString();
                        QString transf = xml.attributes().value("transform").toString();
                        if(!rx.exactMatch(transf))
                        {
                            eMsg = tr("Invalid item: ") + " \"" + pId + "\"";
                            delete scene;
                            return;
                        }

                        qreal x = rx.cap(3).toDouble();
                        qreal y = rx.cap(4).toDouble();
                        qreal cosa = rx.cap(1).toDouble();
                        qreal sina = rx.cap(2).toDouble();

                        bool ok;
                        int etat = xml.attributes().value("etat").toString().toInt(&ok);
                        if(!ok)
                            etat = 1;

                        Place *item = new Place(x, y, etat);
                        item->site2dView = this;
                        item->setPos(QPointF(x, y));
                        item->transfAtt = transf;
                        item->id = pId;
                        item->bloc = xml.attributes().value("block").toString().toInt(&ok);
                        if(!ok)
                        {
                            eMsg = tr("Invalid Bloc element for item ") + " \"" + pId + "\"";
                            delete scene;
                            return;
                        }
                        item->priorite = xml.attributes().value("pri").toString();
                        item->contigu = xml.attributes().value("cont").toString();
                        item->selectorder = xml.attributes().value("selectorder").toString();
                        item->pos = xml.attributes().value("pos").toString();
                        item->row = xml.attributes().value("row").toString();
                        item->a = RadToDeg( qAtan(sina/cosa) );

                        //Actualizar el mapa que relaciona los path con los elementos que este contiene
                        if(pathToPlaceMap.contains(item->row))
                        {
                            pathToPlaceMap[item->row].insert(item->pos.toInt(), item);
                        }
                        else
                        {
                            QMap<int, Place*> posToPlaceMap;
                            posToPlaceMap.insert(item->pos.toInt(), item);
                            pathToPlaceMap.insert(item->row, posToPlaceMap);
                        }

                        QRectF rect = item->boundingRect();
                        qreal wdiv2 = item->boundingRect().width()/2;
                        qreal hdiv2 = item->boundingRect().height()/2;

                        qreal bx = wdiv2/3;
                        qreal by = hdiv2/3;

                        rect.adjust(-bx,-by,bx,by);

                        QTransform translationTransform(1, 0, 0, 1, wdiv2, hdiv2);
                        QTransform rotationTransform(cosa, sina, -sina, cosa, 0, 0);
                        QTransform translationTransform_1(1, 0, 0, 1, -wdiv2, -hdiv2);

                        QTransform transform;
                        transform = translationTransform * rotationTransform * translationTransform_1;

                        QString zone = xml.attributes().value("zone").toString();
                        if(!zone.isEmpty())
                        {
                            QStandardItem *zoneItem = zonesMap.value(zone);
                            if(zoneItem)
                            {
                                QList<Place*> *placesList = zoneItem->data(DataRole::placesinset()).value<QList<Place*>*>();
                                placesList->append(item);
                                item->zoneItem = zoneItem;
                                int cplaces = zoneItem->data(Qt::DisplayRole).toInt();
                                zoneItem->setData(QVariant(cplaces+1), Qt::DisplayRole);
                            }
                        }

                        QString rang;
                        QStringList coorVect;
                        bool cond = false;
                        for(int j=0, m=schemaItem->columnCount(); j<m; j++)
                        {
                            QStandardItem *schemaElemItem = schemaItem->child(0,j);
                            QString nom = schemaElemItem->data(DataRole::nomsitenumbitem()).toString();
                            QString coor = xml.attributes().value(nom).toString();
                            if(nom=="Rang")
                                rang = coor;
                            coorVect << coor;
                            cond |= !coor.isEmpty();
                        }
                        item->coorItem = new QGraphicsSimpleTextItem(coorVect.isEmpty()?"":coorVect.last(), item);
                        QFont font("Times", hdiv2);
                        //font.setStyleStrategy(QFont::OpenGLCompatible);
                        item->coorItem->setFont(font);
                        item->coorItem->setBrush(QBrush(Qt::blue));
                        QRectF txtRect = item->coorItem->boundingRect();
                        item->coorItem->setPos(QPointF(wdiv2 - txtRect.width()/2, 2*hdiv2));

                        item->rowCoorItem = new QGraphicsSimpleTextItem("", item);
                        item->rowCoorItem->setFont(font);
                        item->rowCoorItem->setBrush(QBrush(Qt::green));
                        txtRect = item->rowCoorItem->boundingRect();
                        item->rowCoorItem->setPos(QPointF(-txtRect.width(), hdiv2 - txtRect.height()/2));

                        item->setNumbVect(coorVect);

                        item->setTransform(transform);

                        scene->addItem(item);
                        placeItemsList << item;

                        if(item->bloc)
                        {
                            if(blocMap.contains(item->bloc))
                            {
                                QStandardItem *bStdItem = blocMap.value(item->bloc);
                                item->blocItem = bStdItem;
                                bStdItem = blocItem->child(item->blocItem->row(), 1);
                                int N = bStdItem->data(Qt::DisplayRole).toInt();
                                bStdItem->setData(N+1, Qt::DisplayRole);
                                QList<Place*> *pList = item->blocItem->data(DataRole::placesinset()).value<QList<Place*>*>();
                                pList->append(item);
                            }
                            else
                            {
                                QStandardItem *bStdItem = new QStandardItem;
                                bStdItem->setData(item->bloc, Qt::EditRole);
                                item->blocItem = bStdItem;
                                bStdItem = new QStandardItem;
                                bStdItem->setData(1, Qt::DisplayRole);
                                QList<QStandardItem*> iList;
                                iList << item->blocItem;
                                bStdItem->setEditable(false);
                                iList << bStdItem;
                                QStandardItem *dirItem = new QStandardItem;
                                dirItem->setCheckable(true);
                                dirItem->setCheckState(Qt::Checked);
                                dirItem->setEditable(false);
                                iList << dirItem;
                                blocItem->appendRow(iList);
                                blocMap.insert(item->bloc, item->blocItem);
                                QList<Place*> *pList = new QList<Place*>;
                                pList->append(item);
                                item->blocItem->setData(QVariant::fromValue(pList), DataRole::placesinset());
                            }
                        }

                        if(cond)
                        {
                            QStandardItem *rootItem = numItem;
                            for(int j=0, m=coorVect.size(); j<m; j++)
                            {
                                QString coor = coorVect.at(j);

                                int k, n;
                                for(k=0, n=rootItem->rowCount(); k<n; k++)
                                {
                                    if(rootItem->child(k)->text()==coor)
                                    {
                                        rootItem = rootItem->child(k);
                                        break;
                                    }
                                }

                                if(k==n)
                                {
                                    QStandardItem *newItem = new QStandardItem(coor);
                                    rootItem->appendRow(newItem);
                                    rootItem = newItem;
                                    if(j+1==m)
                                    {
                                        newItem->setData(QVariant::fromValue(item), DataRole::auxptr1());
                                        item->labelItem = newItem;
                                    }
                                }
                            }
                        }
                    }else

                    if(xml.name()=="path" && xml.isStartElement())
                    {
                        endElemCounter = 0;
                        QMap<QString,QString> attValueMap;
                        QXmlStreamAttributes atts = xml.attributes();
                        QString pathId;
                        for(int i=0, n=atts.size(); i<n; i++)
                        {
                            QXmlStreamAttribute att = atts.at(i);
                            QString attName = att.name().toString();
                            QString attValue = att.value().toString();
                            if(attName=="id")
                                pathId = attValue;
                            else
                                attValueMap.insert(attName, attValue);
                        }
                        if(!attValueMap.contains("pos"))
                            attValueMap.insert("pos", QString::number(rowIndex++));
                        svgPathMap.insert(pathId, attValueMap);
                    }else
                        if(xml.isEndElement())
                            endElemCounter++;

                    if(endElemCounter==2)
                        break;
                }
            }else

            if(xml.name()=="g" && xml.isStartElement() && xml.attributes().value("inkscape:label").toString()=="Fond")
            {
                QByteArray hrefBA, outBA;
                QXmlStreamWriter xmlWriter(&outBA);
                xmlWriter.setAutoFormatting(true);
                xmlWriter.writeStartDocument();
                xmlWriter.writeStartElement("svg");

                xmlWriter.writeAttribute("xmlns", "http://www.w3.org/2000/svg");
                xmlWriter.writeAttribute("xmlns:svg", "http://www.w3.org/2000/svg");
                xmlWriter.writeAttribute("xmlns:sodipodi", "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd");
                xmlWriter.writeAttribute("xmlns:inkscape", "http://www.inkscape.org/namespaces/inkscape");
                xmlWriter.writeAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
                xmlWriter.writeAttribute("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
                xmlWriter.writeAttribute("xmlns:cc", "http://creativecommons.org/ns#");
                xmlWriter.writeAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
                xmlWriter.writeAttribute("width", QString::number(docWidth));
                xmlWriter.writeAttribute("height", QString::number(docHeight));

                int endElemCounter = 1, baPos, outBaPos;

                xmlWriter.writeStartElement(xml.name().toString());
                for(int i=0, n=xml.attributes().size(); i<n; i++)
                {
                    QXmlStreamAttribute att = xml.attributes().at(i);
                    xmlWriter.writeAttribute(att.qualifiedName().toString(), att.value().toString());
                }

                QXmlStreamWriter xmlWriter2(&bgBA);
                xmlWriter2.writeStartElement(xml.name().toString());
                for(int i=0, n=xml.attributes().size(); i<n; i++)
                {
                    QXmlStreamAttribute att = xml.attributes().at(i);
                    xmlWriter2.writeAttribute(att.qualifiedName().toString(), att.value().toString());
                }

                bool cond = false;
                while(!xml.atEnd())
                {
                    xml.readNext();
                    if(xml.isStartElement())
                    {
                        endElemCounter = 0;
                        cond = true;
                        xmlWriter.writeStartElement(xml.name().toString());
                        xmlWriter2.writeStartElement(xml.name().toString());
                        for(int i=0, n=xml.attributes().size(); i<n; i++)
                        {
                            QXmlStreamAttribute att = xml.attributes().at(i);
                            QString attName = att.name().toString();
                            QString attValue = att.value().toString();

                            if(attName=="href")
                            {
                                /*for(int i=99; i<attValue.size(); i+=77)
                                    attValue.insert(i, "\n");*/
                                attValue.replace(QRegExp("\\s+"), "\n");
                                hrefBA = attValue.toUtf8();
                                outBaPos = outBA.size();
                                baPos = bgBA.size();
                            }
                            else
                            {
                                xmlWriter.writeAttribute(att.qualifiedName().toString(), attValue);
                                xmlWriter2.writeAttribute(att.qualifiedName().toString(), attValue);
                            }
                        }
                    }
                    else
                        if(xml.isEndElement())
                        {
                            endElemCounter++;
                            xmlWriter.writeEndElement();
                            xmlWriter2.writeEndElement();
                        }

                    if(endElemCounter==2)
                        break;
                }

                xmlWriter.writeEndElement();
                xmlWriter.writeEndDocument();

                xmlWriter2.writeEndElement();

                if(cond)
                {
                    outBA.insert(outBaPos, " xlink:href=\"");
                    outBaPos += 13;
                    outBA.insert(outBaPos, hrefBA);
                    outBaPos += hrefBA.size();
                    outBA.insert(outBaPos, "\"");

                    bgBA.insert(baPos, " xlink:href=\"");
                    baPos += 13;
                    bgBA.insert(baPos, hrefBA);
                    baPos += hrefBA.size();
                    bgBA.insert(baPos, "\"");

                    QSvgRenderer renderer(outBA);
                    QPixmap img(docWidth, docHeight);
                    QPainter painter(&img);
                    renderer.render(&painter);
                    painter.end();

                    fondItem = scene->addPixmap(img);
                    fondItem->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
                    fondItem->setZValue(0.);
                }

                /*QFile f("D:/fondo.svg");
                f.open(QIODevice::WriteOnly);
                f.write(outBA);
                f.close();*/
            }
        }
    }
    else
        eMsg = tr("Invalid file format.");
    if(xml.hasError())
    {
        eMsg = xml.errorString();
    }

    if(eMsg.isEmpty())
    {
        tixrGraphicsView->setScene(scene);

        int i=0, n=blocItem?blocItem->rowCount():0;
        if(n)
        {
            QList<QColor> colorsList = distinctColors(n);
            for(; i<n; i++)
            {
                QStandardItem *stdItem = blocItem->child(i);
                QColor c = colorsList.at(i);
                stdItem->setData(c, Qt::DecorationRole);
                colorList.append(c.name());
            }
        }
    }
}

#ifndef TIXREDITOR
void Site2dView::populateSceneFromJson()
{
    QByteArray respData = ioDev->readAll();
    /*QFile f("D:\\response.json");
    f.open(QIODevice::WriteOnly);
    f.write(respData.constData());
    f.close();*/
    //qDebug("response: %s", respData.constData());

    cJSON *jsonRoot = cJSON_Parse(respData.constData());
    if(!jsonRoot)
    {
        eMsg = QObject::tr(cJSON_GetErrorPtr());
        cJSON_Delete(jsonRoot);
        return;
    }

    if(cJSON_GetArraySize(jsonRoot) != 1)
    {
        Q_ASSERT(true);
        eMsg = QObject::tr("Invalid response");
        cJSON_Delete(jsonRoot);
        return;
    }
    jsonRoot = cJSON_GetArrayItem(jsonRoot, 0);

    //cargar las zonas de tarifas
    cJSON *jsonArray = cJSON_GetObjectItem(jsonRoot, "zonestarincat");
    QMap<int,QStandardItem*> zonesMap;
    int N = jsonArray ? cJSON_GetArraySize(jsonArray) : 0;
    for(int i=0; i<N; i++)
    {
        cJSON *zoneTar = cJSON_GetArrayItem(jsonArray, i);

        QList<QStandardItem*> rowItems;

        cJSON *jsonNom = cJSON_GetObjectItem(zoneTar, "libelle"),
              *jsonCplaces = cJSON_GetObjectItem(zoneTar, "setsize"),
              *jsonCouleur = cJSON_GetObjectItem(zoneTar, "couleur"),
              *jsonTzone = cJSON_GetObjectItem(zoneTar, "tzone");
        QString idUnique(cJSON_GetObjectItem(zoneTar, "idunique")->valuestring);
        int dbId = cJSON_GetObjectItem(zoneTar, "id")->valueint;

        if(jsonNom==0 || jsonCplaces==0 || jsonCouleur==0 || jsonTzone==0)
        {
           eMsg = tr("Invalid file format: wrong zone definition.");
           cJSON_Delete(jsonRoot);
           return;
        }

        QString nom(jsonNom->valuestring);
        int cplaces = jsonCplaces->valueint;
        QString couleur(jsonCouleur->valuestring);
        bool tZone = jsonTzone->valueint;

        QStandardItem *cplacesItem = new QStandardItem;
        cplacesItem->setData(QColor(couleur), Qt::DecorationRole);
        cplacesItem->setData(QVariant::fromValue(new QList<Place*>), DataRole::placesinset());
        cplacesItem->setData(QVariant(tZone?cplaces:0), Qt::DisplayRole);//Si la zona es Placée pongo la cantidad de plazas a cero y más adelante cuento la cantidad real de plazas que pertenecen a la zona
        cplacesItem->setData(nom, DataRole::ztarnom());
        cplacesItem->setData(dbId, DataRole::ikey());

        //actualizar la lista de colores únicos
        if(!colorList.contains(couleur))
        {
            colorList.append(couleur.toLower());
        }

        rowItems << cplacesItem;
        rowItems << new QStandardItem(tZone?tr("Yes"):tr("No"));//True = placée; False = non placée
        rowItems << new QStandardItem(idUnique);

        QStandardItem *prixColumnItem = new QStandardItem;
        prixColumnItem->setCheckable(true);
        rowItems << prixColumnItem;

        zonesItem->appendRow(rowItems);

        zonesStdModel->setVerticalHeaderItem(zonesItem->rowCount()-1, new QStandardItem(nom));
        zonesMap.insert(dbId, cplacesItem);
    }

    CustomGraphicsScene *scene = new CustomGraphicsScene;
    scene->setSite2dView(this);

    //cargar el sistema de numeración
    jsonArray = cJSON_GetObjectItem(jsonRoot, "nsch");//nombres de los componentes del sistema de numeración (Ej. Bloque, Fila, Asiento)
    cJSON *serieArray = cJSON_GetObjectItem(jsonRoot, "ssch");//valores posibles de los componentes del sistema de numeración (Ej. Bloque1, Fila2, Silla5)
    cJSON *levelArray = cJSON_GetObjectItem(jsonRoot, "lsch");//gerarquía de los componentes, 0 nivel más alto, 1 nivel inferior, 2 inferior al 1...

    int nS = jsonArray ? cJSON_GetArraySize(jsonArray) : 0;
    int sS = serieArray ? cJSON_GetArraySize(serieArray) : 0;
    int lS = levelArray ? cJSON_GetArraySize(levelArray) : 0;
    if(nS != sS || sS != lS)
    {
       eMsg = tr("Invalid file format: bad numbering schema.");
       delete scene;
       cJSON_Delete(jsonRoot);
       return;
    }

    QStringList headerLabels;
    for(int j=0; j<nS; j++)
    {
        QString nom = cJSON_GetArrayItem(jsonArray, j)->valuestring;
        QString serie = cJSON_GetArrayItem(serieArray, j)->valuestring;
        int level = cJSON_GetArrayItem(levelArray, j)->valueint;

        if(nom.isEmpty())
        {
           eMsg = tr("Invalid file format: bad numbering schema.");
           delete scene;
           cJSON_Delete(jsonRoot);
           return;
        }

        SerieManager sm(serie);
        if(!sm.isValid())
        {
            eMsg = tr("Invalid file format: bad numbering schema.");
            delete scene;
            cJSON_Delete(jsonRoot);
            return;
        }

        QString desc = QString("Name: %1\nSerie: %2\nSize of serie: %3");
        int N = sm.getSize();
        desc = desc.arg(nom, serie, N>-1?QString::number(N):"INF.");

        QStandardItem *schemaElemItem = new QStandardItem(desc);
        schemaElemItem->setData(nom, DataRole::nomsitenumbitem());
        schemaElemItem->setData(serie, DataRole::seriesitenumbitem());
        schemaElemItem->setData(level, DataRole::levelsitenumbitem());

        int i=0, n=schemaItem->columnCount();
        for(; i<n; i++)
        {
            int l2 = schemaItem->child(0,i)->data(DataRole::levelsitenumbitem()).toInt();
            if(level<l2)
            {
                headerLabels.insert(i, nom);
                schemaItem->insertColumn(i, QList<QStandardItem*>()<<schemaElemItem);
                break;
            }
            else
                if(level==l2)
                {
                   eMsg = tr("Invalid file format: bad numbering schema. <level> value should be unique.");
                   delete scene;
                   cJSON_Delete(jsonRoot);
                   return;
                }
        }
        if(i==n)
        {
            headerLabels.append(nom);
            schemaItem->appendColumn(QList<QStandardItem*>()<<schemaElemItem);
        }
    }
    schemaStdModel->setHorizontalHeaderLabels(headerLabels);

    //cargar el plano
    int rowIndex = 0;
    QRegExp rx("matrix\\(([\\d.\\-eE]+),([\\d.\\-eE]+),[\\d.\\-eE]+,[\\d.\\-eE]+,([\\d.\\-eE]+),([\\d.\\-eE]+)\\)");

    cJSON *jsonVal;
    int docWidth = (jsonVal=cJSON_GetObjectItem(jsonRoot, "ancho"))?jsonVal->valueint:0;
    int docHeight = (jsonVal=cJSON_GetObjectItem(jsonRoot, "alto"))?jsonVal->valueint:0;

    if(!(docWidth && docHeight))
    {
        eMsg = tr("Invalid graphic sizes");
        delete scene;
        cJSON_Delete(jsonRoot);
        return;
    }

    if(progBar)
        progBar->setValue(10);

    jsonArray = cJSON_GetObjectItem(jsonRoot, "prodsforcat");//definiciones de los asientos en el plano

    for(int i=0, n=(jsonArray?cJSON_GetArraySize(jsonArray):0); i<n; i++)
    {
        cJSON *seatJson = cJSON_GetArrayItem(jsonArray, i);

        QString svgId = cJSON_GetObjectItem(seatJson, "svgid")->valuestring;

        QString transf(QByteArray::fromBase64(QByteArray(cJSON_GetObjectItem(seatJson, "transform")->valuestring)));
        if(!rx.exactMatch(transf))
        {
            eMsg = tr("Invalid format for item %1").arg(svgId);
            delete scene;
            cJSON_Delete(jsonRoot);
            return;
        }

        qreal x = rx.cap(3).toDouble();
        qreal y = rx.cap(4).toDouble();
        qreal cosa = rx.cap(1).toDouble();
        qreal sina = rx.cap(2).toDouble();

        jsonVal = cJSON_GetObjectItem(seatJson, "etat");
        int state = jsonVal ? jsonVal->valueint : 1;

        Place *item = new Place(x, y, state);
        item->site2dView = this;
        item->setPos(QPointF(x, y));
        item->transfAtt = transf;
        item->id = svgId;
        item->dbId = cJSON_GetObjectItem(seatJson, "id")->valueint;
        item->bloc = (jsonVal=cJSON_GetObjectItem(seatJson, "block")) ?
                       jsonVal->valueint : -1;
        //item->priorite = cJSON_GetObjectItem(seatJson, "pri")->valuestring;
        //item->contigu = cJSON_GetObjectItem(seatJson, "cont")->valuestring;
        //item->selectorder = QString::number(cJSON_GetObjectItem(seatJson, "selectorder")->valueint);
        item->pos = QString::number(cJSON_GetObjectItem(seatJson, "pos")->valueint);
        item->row = cJSON_GetObjectItem(seatJson, "rowpath")->valuestring;
        item->a = RadToDeg( qAtan(sina/cosa) );

        //Actualizar el mapa que relaciona los path con los elementos que este contiene
        if(pathToPlaceMap.contains(item->row))
        {
            pathToPlaceMap[item->row].insert(item->pos.toInt(), item);
        }
        else
        {
            QMap<int, Place*> posToPlaceMap;
            posToPlaceMap.insert(item->pos.toInt(), item);
            pathToPlaceMap.insert(item->row, posToPlaceMap);
        }

        QRectF rect = item->boundingRect();
        qreal wdiv2 = item->boundingRect().width()/2;
        qreal hdiv2 = item->boundingRect().height()/2;

        qreal bx = wdiv2/3;
        qreal by = hdiv2/3;

        rect.adjust(-bx,-by,bx,by);

        QTransform translationTransform(1, 0, 0, 1, wdiv2, hdiv2);
        QTransform rotationTransform(cosa, sina, -sina, cosa, 0, 0);
        QTransform translationTransform_1(1, 0, 0, 1, -wdiv2, -hdiv2);

        QTransform transform;
        transform = translationTransform * rotationTransform * translationTransform_1;

        int zone = (jsonVal=cJSON_GetObjectItem(seatJson, "zone"))?jsonVal->valueint:-1;
        if(zone >= 0)
        {
            QStandardItem *zoneItem = zonesMap.value(zone);
            if(zoneItem)
            {
                QList<Place*> *placesList = zoneItem->data(DataRole::placesinset()).value<QList<Place*>*>();
                placesList->append(item);
                item->zoneItem = zoneItem;
                int cplaces = zoneItem->data(Qt::DisplayRole).toInt();
                zoneItem->setData(QVariant(cplaces+1), Qt::DisplayRole);
            }
        }

        QString rang;
        QStringList coorVect;
        bool cond = false;
        jsonVal = cJSON_GetObjectItem(seatJson, "coord");
        int m = schemaItem->columnCount();
        if(m != cJSON_GetArraySize(jsonVal))
        {
            eMsg = tr("Invalid coordinates for item %1").arg(svgId);
            delete scene;
            cJSON_Delete(jsonRoot);
            return;
        }
        for(int j=0; j<m; j++)
        {
            QStandardItem *schemaElemItem = schemaItem->child(0,j);
            QString nom = schemaElemItem->data(DataRole::nomsitenumbitem()).toString();
            QString coor = cJSON_GetArrayItem(jsonVal, j)->valuestring;
            if(nom=="Rang")
                rang = coor;
            coorVect << coor;
            cond |= !coor.isEmpty();
        }
        item->coorItem = new QGraphicsSimpleTextItem(coorVect.isEmpty()?"":coorVect.last(), item);
        QFont font("Times", hdiv2);
        //font.setStyleStrategy(QFont::OpenGLCompatible);
        item->coorItem->setFont(font);
        item->coorItem->setBrush(QBrush(Qt::blue));
        QRectF txtRect = item->coorItem->boundingRect();
        item->coorItem->setPos(QPointF(wdiv2 - txtRect.width()/2, 2*hdiv2));

        item->rowCoorItem = new QGraphicsSimpleTextItem("", item);
        item->rowCoorItem->setFont(font);
        item->rowCoorItem->setBrush(QBrush(Qt::green));
        txtRect = item->rowCoorItem->boundingRect();
        item->rowCoorItem->setPos(QPointF(-txtRect.width(), hdiv2 - txtRect.height()/2));

        item->setNumbVect(coorVect);

        item->setTransform(transform);

        scene->addItem(item);
        placeItemsList << item;

        if(item->bloc)
        {
            if(blocMap.contains(item->bloc))
            {
                QStandardItem *bStdItem = blocMap.value(item->bloc);
                item->blocItem = bStdItem;
                bStdItem = blocItem->child(item->blocItem->row(), 1);
                int N = bStdItem->data(Qt::DisplayRole).toInt();
                bStdItem->setData(N+1, Qt::DisplayRole);
                QList<Place*> *pList = item->blocItem->data(DataRole::placesinset()).value<QList<Place*>*>();
                pList->append(item);
            }
            else
            {
                QStandardItem *bStdItem = new QStandardItem;
                bStdItem->setData(item->bloc, Qt::EditRole);
                item->blocItem = bStdItem;
                bStdItem = new QStandardItem;
                bStdItem->setData(1, Qt::DisplayRole);
                QList<QStandardItem*> iList;
                iList << item->blocItem;
                bStdItem->setEditable(false);
                iList << bStdItem;
                QStandardItem *dirItem = new QStandardItem;
                dirItem->setCheckable(true);
                dirItem->setCheckState(Qt::Checked);
                dirItem->setEditable(false);
                iList << dirItem;
                blocItem->appendRow(iList);
                blocMap.insert(item->bloc, item->blocItem);
                QList<Place*> *pList = new QList<Place*>;
                pList->append(item);
                item->blocItem->setData(QVariant::fromValue(pList), DataRole::placesinset());
            }
        }

        if(cond)
        {
            QStandardItem *rootItem = numItem;
            for(int j=0, m=coorVect.size(); j<m; j++)
            {
                QString coor = coorVect.at(j);

                int k, n;
                for(k=0, n=rootItem->rowCount(); k<n; k++)
                {
                    if(rootItem->child(k)->text()==coor)
                    {
                        rootItem = rootItem->child(k);
                        break;
                    }
                }

                if(k==n)
                {
                    QStandardItem *newItem = new QStandardItem(coor);
                    rootItem->appendRow(newItem);
                    rootItem = newItem;
                    if(j+1==m)
                    {
                        newItem->setData(QVariant::fromValue(item), DataRole::auxptr1());
                        item->labelItem = newItem;
                    }
                }
            }
        }
    }

    //cargar los trazos de las filas de asientos
    jsonArray = cJSON_GetObjectItem(jsonRoot, "svgp");//svg paths
    for(int i=0, n=(jsonArray?cJSON_GetArraySize(jsonArray):0); i<n; i++)
    {
        svgPathList << QString(QByteArray::fromBase64(QByteArray(cJSON_GetArrayItem(jsonArray, i)->valuestring)));
    }

    //cargar el fondo
    jsonVal = cJSON_GetObjectItem(jsonRoot, "svgb");
    if(jsonVal)
    {
        QSvgRenderer renderer(QByteArray::fromBase64(QByteArray(jsonVal->valuestring)));
        QPixmap img(docWidth, docHeight);
        QPainter painter(&img);
        renderer.render(&painter);
        painter.end();

        fondItem = scene->addPixmap(img);
        fondItem->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
        fondItem->setZValue(0.);

        /*QFile f("D:/fondo.svg");
        f.open(QIODevice::WriteOnly);
        f.write(outBA);
        f.close();*/
    }

    if(eMsg.isEmpty())
    {
        tixrGraphicsView->setScene(scene);

        int i=0, n=blocItem?blocItem->rowCount():0;
        if(n)
        {
            QList<QColor> colorsList = distinctColors(n);
            for(; i<n; i++)
            {
                QStandardItem *stdItem = blocItem->child(i);
                QColor c = colorsList.at(i);
                stdItem->setData(c, Qt::DecorationRole);
                colorList.append(c.name());
            }
        }
    }
}

#ifndef TIXREDITOR
void Site2dView::prixItemChecked(QStandardItem * item)
{
    if(item->isCheckable())
    {
        if(item->text().isEmpty())
            item->setCheckState(Qt::Unchecked);
        else
        {
            QStandardItem *hItem = zonesStdModel->verticalHeaderItem(item->row());
            hItem->setData(QVariant(), DataRole::ikey());
            hItem->setData(QVariant(), DataRole::ztarnom());
            item->setText("");
            item->setCheckState(Qt::Unchecked);
        }
    }
}
#endif

bool Site2dView::save(QString fileName)
{
    if(fileName.isEmpty())
        fileName = svgFile;

    QFile f(fileName);
    if(!f.open(QIODevice::WriteOnly))
    {
        eMsg = tr("Error creating output file");
        return false;
    }

    QByteArray resultBA;
    QXmlStreamWriter xmlWriter(&resultBA);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("svg");

    xmlWriter.writeAttribute("xmlns", "http://www.w3.org/2000/svg");
    xmlWriter.writeAttribute("xmlns:svg", "http://www.w3.org/2000/svg");
    xmlWriter.writeAttribute("xmlns:sodipodi", "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd");
    xmlWriter.writeAttribute("xmlns:inkscape", "http://www.inkscape.org/namespaces/inkscape");
    xmlWriter.writeAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
    xmlWriter.writeAttribute("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
    xmlWriter.writeAttribute("xmlns:cc", "http://creativecommons.org/ns#");
    xmlWriter.writeAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
    xmlWriter.writeAttribute("xmlns:SantRosSoft", "http://www.santrossoft.com/xml");
    xmlWriter.writeAttribute("width", QString::number(int(tixrGraphicsView->scene()->width())));
    xmlWriter.writeAttribute("height", QString::number(int(tixrGraphicsView->scene()->height())));
    xmlWriter.writeAttribute("id", "tixrVenuePlan");
    xmlWriter.writeAttribute("sodipodi:docname", "TixRealm Venue Plan");

    xmlWriter.writeStartElement("sodipodi:namedview");
    xmlWriter.writeAttribute("inkscape:document-units", "px");
    xmlWriter.writeAttribute("id", "base");
    xmlWriter.writeAttribute("pagecolor", "#ffffff");
    xmlWriter.writeAttribute("bordercolor", "#666666");
    xmlWriter.writeAttribute("borderopacity", "1.0");
    xmlWriter.writeAttribute("inkscape:pageopacity", "0.0");
    xmlWriter.writeAttribute("inkscape:showpageshadow", "false");
    xmlWriter.writeAttribute("inkscape:zoom", "1");
    xmlWriter.writeAttribute("inkscape:cx", QString::number(tixrGraphicsView->scene()->width()/2));
    xmlWriter.writeAttribute("inkscape:cy", QString::number(tixrGraphicsView->scene()->height()/2));
    xmlWriter.writeAttribute("inkscape:current-layer", "places");
    xmlWriter.writeAttribute("units", "px");
    xmlWriter.writeAttribute("showgrid", "false");
    xmlWriter.writeAttribute("inkscape:window-width", "1920");
    xmlWriter.writeAttribute("inkscape:window-height", "976");
    xmlWriter.writeAttribute("inkscape:window-x", "-8");
    xmlWriter.writeAttribute("inkscape:window-y", "-8");
    xmlWriter.writeAttribute("inkscape:window-maximized", "1");
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("metadata");
    xmlWriter.writeAttribute("id", "metadata1");
    xmlWriter.writeStartElement("TixRealm");
    xmlWriter.writeStartElement("Zones");
    for(int j=0, m=zonesItem->rowCount(); j<m; j++)
    {
        xmlWriter.writeStartElement("Zone");
        QStandardItem *headerZItem = zonesStdModel->verticalHeaderItem(j);
        xmlWriter.writeAttribute("nom", headerZItem->text());
        QStandardItem *zItem = zonesItem->child(j);
        xmlWriter.writeAttribute("cplaces", zItem->text());
        QColor zColor = zItem->data(Qt::DecorationRole).value<QColor>();
        xmlWriter.writeAttribute("couleur", zColor.name());
        xmlWriter.writeAttribute("tZone", zonesItem->child(j,1)->text().compare(tr("Yes"))?"0":"1");
        xmlWriter.writeAttribute("idUnique", zonesItem->child(j,2)->text());
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("SchemaNum");
    for(int j=0, m=schemaItem->columnCount(); j<m; j++)
    {
        QStandardItem *stdItem = schemaItem->child(0,j);
        xmlWriter.writeStartElement("SchemaElem");
        xmlWriter.writeAttribute("nom", stdItem->data(DataRole::nomsitenumbitem()).toString());
        xmlWriter.writeAttribute("serie", stdItem->data(DataRole::seriesitenumbitem()).toString());
        xmlWriter.writeAttribute("level", stdItem->data(DataRole::levelsitenumbitem()).toString());
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("g");
    xmlWriter.writeAttribute("id", "images");
    xmlWriter.writeAttribute("inkscape:groupmode", "layer");
    xmlWriter.writeAttribute("inkscape:label", "Images");
    xmlWriter.writeEndElement();

    /*xmlWriter.writeStartElement("g");
    xmlWriter.writeAttribute("id", "fond");
    xmlWriter.writeAttribute("inkscape:groupmode", "layer");
    xmlWriter.writeAttribute("inkscape:label", "Fond");
    int baPos = resultBA.size();
    xmlWriter.writeEndElement();*/
    int baPos = resultBA.size();//Guardar la pos. para insertar el fondo al final.
    //QXmlStreamWriter tiene problemas para escribir atributos excesivamente grandes como es el caso en las imágenes empotradas (atributo xlink:href)

    xmlWriter.writeStartElement("g");
    xmlWriter.writeAttribute("id", "notes");
    xmlWriter.writeAttribute("inkscape:groupmode", "layer");
    xmlWriter.writeAttribute("inkscape:label", "Notes");
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("g");
    xmlWriter.writeAttribute("id", "places");
    xmlWriter.writeAttribute("inkscape:groupmode", "layer");
    xmlWriter.writeAttribute("inkscape:label", "Places");

    for(QMap<QString, QMap<QString,QString> >::const_iterator it2 = svgPathMap.constBegin(); it2 != svgPathMap.constEnd(); it2++)
    {
        xmlWriter.writeStartElement("path");
        xmlWriter.writeAttribute("id", it2.key());
        QMap<QString,QString> attsValueMap = it2.value();
        for(QMap<QString,QString>::const_iterator it = attsValueMap.constBegin(); it != attsValueMap.constEnd(); it++)
        {
            xmlWriter.writeAttribute(it.key(), it.value());
        }
        xmlWriter.writeEndElement();
    }

    for(int i=0, n=placeItemsList.size(); i<n; i++)
    {
        Place *place = placeItemsList.at(i);

        xmlWriter.writeStartElement("use");
        xmlWriter.writeAttribute("id", place->id);
        xmlWriter.writeAttribute("transform", place->transfAtt);
        xmlWriter.writeAttribute("xlink:href", "#tixR");

        if(place->zoneItem)
            xmlWriter.writeAttribute("zone", place->zoneItem->data(DataRole::ztarnom()).toString());

        xmlWriter.writeAttribute("block", QString::number(place->bloc));
        xmlWriter.writeAttribute("pri", place->priorite);
        xmlWriter.writeAttribute("cont", place->contigu);
        xmlWriter.writeAttribute("selectorder", place->selectorder);
        xmlWriter.writeAttribute("row", place->row);
        xmlWriter.writeAttribute("pos", place->pos);

        QStringList coordVect = place->numbVect();
        for(int j=0, m=schemaItem->columnCount(); j<m; j++)
        {
            QStandardItem *stdItem = schemaItem->child(0,j);
            xmlWriter.writeAttribute(stdItem->data(DataRole::nomsitenumbitem()).toString(), coordVect.at(j));
        }

        xmlWriter.writeEndElement();
    }

    xmlWriter.writeEndElement();// layer Places

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();

    if(fondItem)
    {
        resultBA.insert(baPos, bgBA);
    }

    f.write(resultBA);
    f.close();

    return true;
}

void Site2dView::exportAsJson(QJsonObject &jsonObj, QString &error)
{
    QGraphicsScene *scene;
    if(!(tixrGraphicsView && (scene = tixrGraphicsView->scene())))
    {
        error = tr("Invalid venue plan");
        return;
    }

    jsonObj.insert("svgfile", QJsonValue(getSiteDialogName()));

    jsonObj.insert("width", QJsonValue(scene->width()));
    jsonObj.insert("height", QJsonValue(scene->height()));

    //exportar el esquema de numeración
    QJsonArray nameArray;
    QJsonArray serieArray;
    QJsonArray levelArray;
    for(int i=0, n=schemaItem->columnCount(); i<n; i++)
    {
        QStandardItem *schemaElemItem = schemaItem->child(0,i);
        nameArray << QJsonValue(schemaElemItem->data(DataRole::nomsitenumbitem()).toString());
        serieArray << QJsonValue(schemaElemItem->data(DataRole::seriesitenumbitem()).toString());
        levelArray << QJsonValue(schemaElemItem->data(DataRole::levelsitenumbitem()).toInt());
    }

    jsonObj.insert("nsch", nameArray);
    jsonObj.insert("ssch", serieArray);
    jsonObj.insert("lsch", levelArray);

    //exportar los trazos de las filas
    QJsonArray rowPathsArray;
    for(QMap<QString, QMap<QString,QString> >::const_iterator it2 = svgPathMap.constBegin(); it2 != svgPathMap.constEnd(); it2++)
    {
        QString path = QString("<path id=\"%1\"").arg(it2.key());
        QMap<QString,QString> attsValueMap = it2.value();
        for(QMap<QString,QString>::const_iterator it = attsValueMap.constBegin(); it != attsValueMap.constEnd(); it++)
        {
            path +=QString(" %1=\"%2\"").arg(it.key(), it.value());
        }

        rowPathsArray << QJsonValue(QString(path.toUtf8().toBase64()));
    }
    jsonObj.insert("paths", QJsonValue(rowPathsArray));

    //tariff zones
    QJsonArray zonesArray;
    for(int j=0, m=zonesItem->rowCount(); j<m; j++)
    {
        QJsonObject zoneObj;
        QStandardItem *headerZItem = zonesStdModel->verticalHeaderItem(j);
        zoneObj.insert("name", headerZItem->text());
        QStandardItem *zItem = zonesItem->child(j);
        zoneObj.insert("size", zItem->text().toInt());
        QColor zColor = zItem->data(Qt::DecorationRole).value<QColor>();
        zoneObj.insert("color", zColor.name());
        zoneObj.insert("type", zonesItem->child(j,1)->text().compare(tr("Yes"))?0:1);
        zonesArray << zoneObj;
    }
    jsonObj.insert("zones", QJsonValue(zonesArray));

    //exportar el plano
    QJsonArray seatsArray;
    for(int i=0, n=placeItemsList.size(); i<n; i++)
    {
        const Place *seat = placeItemsList.at(i);
        QJsonObject seatObj;
        seatObj.insert("id", QJsonValue(seat->id));
        seatObj.insert("transform", QJsonValue(QString(seat->transfAtt.toUtf8().toBase64())));
        if(seat->zoneItem)
            seatObj.insert("zone", QJsonValue(seat->zoneItem->data(DataRole::ztarnom()).toString()));

        seatObj.insert("block", QJsonValue(seat->bloc));
        seatObj.insert("pri", QJsonValue(seat->priorite));
        seatObj.insert("cont", QJsonValue(seat->contigu));
        seatObj.insert("selectorder", QJsonValue(seat->selectorder));
        seatObj.insert("row", QJsonValue(seat->row));
        seatObj.insert("pos", QJsonValue(seat->pos));
        seatObj.insert("totaldispo", QJsonValue(1));

        //numeración
        seatObj.insert("coord", QJsonValue(QJsonArray::fromStringList(seat->numbVect())));

        seatsArray << seatObj;
    }
    jsonObj.insert("seats", QJsonValue(seatsArray));

    //background
    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QIODevice::WriteOnly);
    bool r = fondItem->pixmap().save(&buffer, "PNG");
    buffer.close();
    Q_ASSERT(r);
    jsonObj.insert("bg", QJsonValue(QString(ba.toBase64())));
}

QString Site2dView::getSiteDialogName()
{
    QFileInfo fi(svgFile);
    return fi.baseName();
}

void Site2dView::on_resetButton_clicked()
{
    zoomSlider->setValue(zoomSlider->minimum());
    rotateSlider->setValue(0);
    setupMatrix();
    tixrGraphicsView->ensureVisible(QRectF(0, 0, 0, 0));

    resetButton->setEnabled(false);
}

bool Site2dView::viewportEvent(QEvent *event)
{
    tixrGraphicsView->setDragMode(QGraphicsView::NoDrag);
    switch (event->type())
    {
        case QEvent::TouchBegin:
        case QEvent::TouchUpdate:
        case QEvent::TouchEnd:
        {
            QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);
            QList<QTouchEvent::TouchPoint> touchPoints = touchEvent->touchPoints();
            //qDebug(QString::number(touchPoints.count()).toUtf8().constData());
            if(touchPoints.count() == 2)
            {
                // determine scale factor
                const QTouchEvent::TouchPoint &touchPoint0 = touchPoints.first();
                const QTouchEvent::TouchPoint &touchPoint1 = touchPoints.last();
                qreal l1 = QLineF(touchPoint0.pos(), touchPoint0.lastPos()).length();
                qreal l2 = QLineF(touchPoint1.pos(), touchPoint1.lastPos()).length();
                qreal th = 5;
                if(l1>th && l2>th)//zoom
                {
                    qreal currentScaleFactor =
                            (QLineF(touchPoint0.pos(), touchPoint1.pos()).length()
                            / QLineF(touchPoint0.lastPos(), touchPoint1.lastPos()).length());
                    qreal tmpTotalScaleFactor = totalScaleFactor;
                    if(touchEvent->touchPointStates() & Qt::TouchPointReleased)
                    {
                        // if one of the fingers is released, remember the current scale
                        // factor so that adding another finger later will continue zooming
                        // by adding new scale factor to the existing remembered value.
                        tmpTotalScaleFactor *= currentScaleFactor;
                        currentScaleFactor = 1;
                    }
                    //qreal f = tmpTotalScaleFactor * currentScaleFactor;
                    //zoomScene(f);
                }
                else//rotation
                {
                    int a = 0;
                    if(l1<=th && l2>th)
                        a = QLineF(touchPoint0.lastPos(), touchPoint1.lastPos()).angleTo(QLineF(touchPoint0.pos(), touchPoint1.pos()));
                    else
                        if(l1>th && l2<=th)
                            a = QLineF(touchPoint1.lastPos(), touchPoint0.lastPos()).angleTo(QLineF(touchPoint1.pos(), touchPoint0.pos()));
                    totalAngle += a;
                    //rotScene(-totalAngle);
                    //qDebug(("Calculated: "+QString::number(a)).toUtf8().constData());
                    //qDebug(("Rotated: "+QString::number(-totalAngle)).toUtf8().constData());
                }
            }
            tixrGraphicsView->setDragMode(QGraphicsView::RubberBandDrag);
            return true;
        }
        default:
            break;
    }
    tixrGraphicsView->setDragMode(QGraphicsView::RubberBandDrag);
    return false;//QGraphicsView::viewportEvent(event);
}

void Site2dView::setResetButtonEnabled()
{
    resetButton->setEnabled(true);
}

void Site2dView::setupMatrix()
{
    if(mapNavigationWindow)
    {//desactivar la actualización de la posición en el mini-map, para evitar la recursión
        QObject::disconnect(tixrGraphicsView->horizontalScrollBar(), SIGNAL(valueChanged(int)), mapNavigationWindow, SLOT(updateHorFramePos(int)));
        QObject::disconnect(tixrGraphicsView->verticalScrollBar(), SIGNAL(valueChanged(int)), mapNavigationWindow, SLOT(updateVerFramePos(int)));
    }

    scaleIndex = zoomSlider->value();
    qreal scale = qPow(qreal(2), (scaleIndex/* - 250*/) / qreal(50));

    QMatrix matrix;
    matrix.scale(scale, scale);

    updateImgCache((scaleIndex / ZOOM_UNIT)*ZOOM_UNIT);

    matrix.rotate(rotateSlider->value());

    tixrGraphicsView->setMatrix(matrix);
    setResetButtonEnabled();

    emit sceneRotated(rotateSlider->value());

    if(mapNavigationWindow)
    {//reactivar la actualización de la posición en el mini-map
        QObject::connect(tixrGraphicsView->horizontalScrollBar(), SIGNAL(valueChanged(int)), mapNavigationWindow, SLOT(updateHorFramePos(int)), Qt::UniqueConnection);
        QObject::connect(tixrGraphicsView->verticalScrollBar(), SIGNAL(valueChanged(int)), mapNavigationWindow, SLOT(updateVerFramePos(int)), Qt::UniqueConnection);
    }
}

void Site2dView::updateImgCache(int scaleIdx)
{
    //si es necesario, inicializar la caché
    if(stateIconsCache->isEmpty())
    {
        for(int i=0; i<statesLimit; i++)
        {
            stateIconsCache->append( QMap<int, QMap<QString, QPair<QImage, QImage> > >() );
        }
    }

    qreal scale = qPow(qreal(2), scaleIdx / qreal(50));

    for(int i=0; i<statesLimit; i++)
    {
        QFile f(QString(":/seatimg/place%1.svg").arg(i+1));
        Q_ASSERT(f.open(QIODevice::ReadOnly));
        QDomDocument domDoc;
        domDoc.setContent(&f);
        f.close();
        QList<QDomElement> targetElemList;

        QFile f2(QString(":/seatimg/place%1Selec.svg").arg(i+1));
        Q_ASSERT(f2.open(QIODevice::ReadOnly));
        QDomDocument domDoc2;
        domDoc2.setContent(&f2);
        f2.close();
        QList<QDomElement> targetElemList2;

        QMatrix transfMtx;
        transfMtx.scale(scale, scale);

        QMap<int, QMap<QString, QPair<QImage, QImage> > > &stateEntry = (*stateIconsCache)[i];

        if(!stateEntry.contains(scaleIdx))
        {
            stateEntry.insert(scaleIdx, QMap<QString, QPair<QImage, QImage> >());
        }

        QMap<QString, QPair<QImage, QImage> > &scaleEntry = stateEntry[scaleIdx];

        for(int j=0, n=colorList.size(); j<n; j++)
        {
            QString cname = colorList.at(j);

            if(!scaleEntry.contains(cname))
            {
                if(targetElemList.isEmpty())//inicializar la lista de elementos de path svg cuyo color debe ser actualizado
                {
                    QDomNodeList nodeList = domDoc.elementsByTagName("path");
                    for(int l=0,n=nodeList.size(); l<n; l++)
                    {
                        QDomElement elem = nodeList.at(l).toElement();
                        Q_ASSERT(!elem.isNull());

                        if(elem.attribute("id").contains("scCit"))
                        {
                            targetElemList.append(elem);
                        }
                    }
                }

                bool c = targetElemList2.isEmpty();
                if(c)
                {
                    QDomNodeList nodeList = domDoc2.elementsByTagName("path");
                    for(int l=0,n=nodeList.size(); l<n; l++)
                    {
                        QDomElement elem = nodeList.at(l).toElement();
                        Q_ASSERT(!elem.isNull());

                        if(elem.attribute("id").contains("scCit") && c)
                        {
                            targetElemList2.append(elem);
                        }
                    }
                }

                //actualizar el color de los paths
                for(int k=0, m=targetElemList.size(); k<m; k++)
                {
                    QDomElement elem = targetElemList.at(k);
                    elem.setAttribute("fill", cname);//!<Luego de aplicar la herramienta de simplificación svg es mucho más sencillo actualizar el color (Gracias a Jerôme)
                }

                //actualizar el color de los paths
                for(int k=0, m=targetElemList2.size(); k<m; k++)
                {
                    QDomElement elem = targetElemList2.at(k);
                    elem.setAttribute("fill", cname);//!<Luego de aplicar la herramienta de simplificación svg es mucho más sencillo actualizar el color (Gracias a Jerôme)
                }

                QSvgRenderer renderer(domDoc.toByteArray());

                QImage img(16, 13, QImage::Format_ARGB32);
                img.fill(0);
                img = img.transformed(transfMtx);

                QPainter painter(&img);
                renderer.render(&painter);
                painter.end();

                QSvgRenderer renderer2(domDoc2.toByteArray());

                QImage img2(16, 13, QImage::Format_ARGB32);
                img2.fill(0);
                img2 = img2.transformed(transfMtx);

                QPainter painter2(&img2);
                renderer2.render(&painter2);
                painter2.end();

                scaleEntry.insert(cname, QPair<QImage, QImage>(img, img2));
            }
        }
    }
}

QImage *Site2dView::getCachedImg(int state, QString color, bool selected)
{
    if(state<1 || state>stateIconsCache->size())
        return 0;

    QMap<int, QMap<QString, QPair<QImage, QImage> > > &stateEntry = (*stateIconsCache)[state-1];

    int scaleIdx = (scaleIndex / ZOOM_UNIT)*ZOOM_UNIT;

    if(!stateEntry.contains(scaleIdx))
        return 0;

    QMap<QString, QPair<QImage, QImage> > &scaleEntry = stateEntry[scaleIdx];

    if(!scaleEntry.contains(color))
        return 0;

    return &(selected?scaleEntry[color].second:scaleEntry[color].first);
}

void Site2dView::on_tbZoomIn_clicked()
{
    zoomSlider->setValue(zoomSlider->value() + 1);
}

void Site2dView::on_tbZoomOut_clicked()
{
    zoomSlider->setValue(zoomSlider->value() - 1);
}

void Site2dView::on_tbRotateLeft_clicked()
{
    rotateSlider->setValue(rotateSlider->value() - 10);
}

void Site2dView::on_tbRotateRight_clicked()
{
    rotateSlider->setValue(rotateSlider->value() + 10);
}

void Site2dView::setToSave()
{
    toSave = true;
    setWindowTitle(getSiteDialogName()+"*");
}

void Site2dView::setSaved()
{
    toSave = false;
    setWindowTitle(getSiteDialogName());
}

void Site2dView::changeFile()
{
    tixrGraphicsView->scene()->clear();
    populateScene();
}

void Site2dView::removeFileWatcher()
{
#ifndef QT_NO_FILESYSTEMWATCHER
    if(fileWatcher)
        fileWatcher->removePath(svgFile);
#endif
}

void Site2dView::updateFileWatcher()
{
#ifndef QT_NO_FILESYSTEMWATCHER
    if(fileWatcher)
        fileWatcher->addPath(svgFile);
#endif
}

void Site2dView::on_tbAddZone_clicked()
{
    ZoneDialog zoneDialog(this);
    zoneDialog.setWindowModality(Qt::WindowModal);
    int result = zoneDialog.exec();
    if(result)
    {
        QList<QStandardItem*> rowItems;

        QStandardItem *cplacesItem = new QStandardItem;
        cplacesItem->setData(QVariant(zoneDialog.cplaces), Qt::DisplayRole);
        cplacesItem->setData(QColor(zoneDialog.couleur), Qt::DecorationRole);
        cplacesItem->setData(QVariant::fromValue(new QList<Place*>), DataRole::placesinset());
        rowItems << cplacesItem;

        rowItems << new QStandardItem(zoneDialog.tzone?tr("Yes"):tr("No"));
        rowItems << new QStandardItem(zoneDialog.numUnique);
        QStandardItem *prixColumnItem = new QStandardItem;
        prixColumnItem->setCheckable(true);
        rowItems << prixColumnItem;
        zonesItem->appendRow(rowItems);

        zonesStdModel->setVerticalHeaderItem(zonesItem->rowCount()-1, new QStandardItem(zoneDialog.nom));

        colorList.append(zoneDialog.couleur);
        updateImgCache();
    }
}

void Site2dView::on_tbDelZone_clicked()
{
    QModelIndexList indexList = tableView->selectionModel()->selectedRows();
    if(indexList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord selectionner une zone."));
        return;
    }

    QModelIndex idx = indexList.at(0);//sólo se puede seleccionar una zona a la vez
    QList<Place*> *placesList = zonesItem->child(idx.row())->data(DataRole::placesinset()).value<QList<Place*>*>();

    for(int i=0, n=placesList->size(); i<n; i++)
    {
        placesList->at(i)->zoneItem = 0;
        placesList->at(i)->update();
    }

    delete placesList;

    QStringList headerLabels;
    for(int i=0, n=zonesItem->rowCount(); i<n; i++)
    {
        headerLabels << zonesStdModel->verticalHeaderItem(i)->text();
    }

    zonesItem->removeRow(idx.row());
    headerLabels.removeAt(idx.row());

    zonesStdModel->setVerticalHeaderLabels(headerLabels);
}

void Site2dView::on_tbAddReservoir_clicked()
{
    ReservoirDialog reserDialog(this);
    reserDialog.setWindowModality(Qt::WindowModal);
    int result = reserDialog.exec();
    if(result)
    {
        QList<QStandardItem*> rowItems;

        QStandardItem *cplacesItem = new QStandardItem;
        cplacesItem->setData(QVariant(reserDialog.cplaces), Qt::DisplayRole);
        cplacesItem->setData(QColor(reserDialog.couleur), Qt::DecorationRole);
        cplacesItem->setData(QVariant::fromValue(new QList<Place*>), DataRole::placesinset());
        rowItems << cplacesItem;

        rowItems << new QStandardItem(reserDialog.tzone?tr("Yes"):tr("No"));
        rowItems << new QStandardItem(reserDialog.numUnique);
        QStandardItem *prixColumnItem = new QStandardItem;
        prixColumnItem->setCheckable(true);
        rowItems << prixColumnItem;
        zonesItem->appendRow(rowItems);

        zonesStdModel->setVerticalHeaderItem(zonesItem->rowCount()-1, new QStandardItem(reserDialog.nom));
    }
}

void Site2dView::on_tbDelReservoir_clicked()
{
    QModelIndexList indexList = tableView->selectionModel()->selectedRows();
    if(indexList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord selectionner une zone."));
        return;
    }

    QModelIndex idx = indexList.at(0);//sólo se puede seleccionar una zona a la vez
    QList<Place*> *placesList = zonesItem->child(idx.row())->data(DataRole::placesinset()).value<QList<Place*>*>();

    for(int i=0, n=placesList->size(); i<n; i++)
    {
        placesList->at(i)->zoneItem = 0;
        placesList->at(i)->update();
    }

    delete placesList;

    QStringList headerLabels;
    for(int i=0, n=zonesItem->rowCount(); i<n; i++)
    {
        headerLabels << zonesStdModel->verticalHeaderItem(i)->text();
    }

    zonesItem->removeRow(idx.row());
    headerLabels.removeAt(idx.row());

    zonesStdModel->setVerticalHeaderLabels(headerLabels);
}

void Site2dView::showVisualInfo(int categ)//Visualiza las zones tarifs, numérotation o bloc de remplissage automatique.
{
    tixrGraphicsView->scene()->blockSignals(true);
    categInfoToShow = categ;
    switch(categ)
    {
    case 1:
        selectedZones = tableView->selectionModel()->selectedRows();
        break;

    case 2:
        if(selectedBlocs.size()==1)//Mostrar el orden de la fila en el primer item de la fila que pertenece al bloque seleccionado
        {
            QList<Place*> *pPlaceList = blocsStdModel->data(selectedBlocs.first(), 33).value<QList<Place*>*>();
            QList<QString> rangList = orderOfRangInBloc.value( blocsStdModel->data(selectedBlocs.first(), Qt::EditRole).toInt() );

            rowToFirstPlaceInBlocMap.clear();
            if(pPlaceList && !pPlaceList->isEmpty() && !rangList.isEmpty())
            {
                //TODO: Basado en el orden relativo de las filas en cada bloque encontrar la primera plaza en cada fila dentro del bloque.
                for(int i=0, n=pPlaceList->size(); i<n; i++)
                {
                    Place *place = pPlaceList->at(i);
                    if(rowToFirstPlaceInBlocMap.contains(place->row))
                    {
                        Place *place2 = rowToFirstPlaceInBlocMap.value(place->row);
                        if(place->pos.toInt() < place2->pos.toInt())
                            rowToFirstPlaceInBlocMap.insert(place->row, place);
                    }
                    else
                        rowToFirstPlaceInBlocMap.insert(place->row, place);
                }
            }

        }
        break;

    case 3:;
    }

    tixrGraphicsView->setDragMode(categ==2?QGraphicsView::NoDrag:QGraphicsView::RubberBandDrag);

    tixrGraphicsView->scene()->blockSignals(false);
    tixrGraphicsView->scene()->update();
}

void Site2dView::on_tableView_clicked(const QModelIndex & /*index*/)
{
    showVisualInfo(1);
}

void Site2dView::on_tableView_2_clicked(const QModelIndex & index)
{
    tbAddNumLevel->setEnabled(index.column()<schemaItem->columnCount()-1);
    tbDelNumLevel->setEnabled(index.column()<schemaItem->columnCount()-2);
}

void Site2dView::genScaledPict(QString fileName)
{
    QFileInfo fi(fileName);

    QSvgRenderer svgRend(fileName);
    for(int i=4; i<11; i++)
    {
        int n = qPow(2,i);
        QImage img(n, n, QImage::Format_RGB32);
        QPainter painter(&img);
        svgRend.render(&painter);
        img.save(QString("%1%2x%2.png").arg(fi.baseName()).arg(n));
    }
}

QList<QGraphicsItem*> Site2dView::getSelItems()
{
    return tixrGraphicsView->scene()->selectedItems();
}

void Site2dView::on_pbAddPlaces_clicked()
{
    QList<QGraphicsItem*> selItems = tixrGraphicsView->scene()->selectedItems();
    if(selItems.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner des places."));
        return;
    }

    QModelIndexList indexList = tableView->selectionModel()->selectedRows();
    if(indexList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner une zone."));
        return;
    }

    QModelIndex idx = indexList.at(0);
    QStandardItem *zoneItem = zonesItem->child(idx.row());
    QList<Place*> *placesList = zonesItem->child(idx.row())->data(DataRole::placesinset()).value<QList<Place*>*>();

    QColor zColor = zoneItem->data(Qt::DecorationRole).value<QColor>();

    for(int i=0, n=selItems.size(); i<n; i++)
    {
        Place *item = (Place*)selItems.at(i);
        if(item->zoneItem==zoneItem)
            continue;

        placesList->append(item);

        if(item->zoneItem)
        {
            QList<Place*> *placesList2 = zonesItem->child(item->zoneItem->index().row())->data(DataRole::placesinset()).value<QList<Place*>*>();
            placesList2->removeAll(item);
            QStandardItem *cplacesItem = zonesItem->child(item->zoneItem->index().row(), 1);
            cplacesItem->setData(QVariant(placesList2->size()), Qt::DisplayRole);
        }

        item->zoneItem = zoneItem;
    }

    QStandardItem *cplacesItem = zonesItem->child(idx.row(), 1);
    cplacesItem->setData(QVariant(placesList->size()), Qt::DisplayRole);
}

void Site2dView::on_pbDelPlaces_clicked()
{
    QList<QGraphicsItem*> selItems = tixrGraphicsView->scene()->selectedItems();
    if(selItems.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner des places."));
        return;
    }

    for(int i=0, n=selItems.size(); i<n; i++)
    {
        Place *item = (Place*)selItems.at(i);
        if(!item->zoneItem)
            continue;

        QList<Place*> *placesList = zonesItem->child(item->zoneItem->index().row())->data(DataRole::placesinset()).value<QList<Place*>*>();
        placesList->removeAll(item);

        QStandardItem *cplacesItem = zonesItem->child(item->zoneItem->index().row(), 1);
        cplacesItem->setData(QVariant(placesList->size()), Qt::DisplayRole);

        item->zoneItem = 0;
    }
}

void Site2dView::editSchemaElem(const QModelIndex & index)
{
    SchemaNumDialog schemaNumDialog(this);
    schemaNumDialog.setWindowModality(Qt::WindowModal);

    QString nom = schemaStdModel->data(index, DataRole::nomsitenumbitem()).toString();
    QString serie = schemaStdModel->data(index, DataRole::seriesitenumbitem()).toString();

    schemaNumDialog.setNom(nom);
    schemaNumDialog.setSerie(serie);

    int result = schemaNumDialog.exec();
    if(result)
    {
        nom = schemaNumDialog.nom();
        serie = schemaNumDialog.serie();

        QString desc = QString("Nom: %1\nSérie: %2\nTaille de la série: %3");
        SerieManager sm(serie);
        int N = sm.getSize();
        desc = desc.arg(nom, serie, N>-1?QString::number(N):"INF.");

        schemaStdModel->setData(index, desc, Qt::DisplayRole);
        schemaStdModel->setData(index, nom, DataRole::nomsitenumbitem());
        schemaStdModel->setData(index, serie, DataRole::seriesitenumbitem());
    }
}

void Site2dView::on_tbNumerate_clicked()
{
    msgLog.clear();
    QModelIndexList idxList = columnView->selectionModel()->selectedIndexes();
    if(idxList.isEmpty())
    {
        for(int i=0, n=numItem->rowCount(); i<n; i++)
            idxList << numItem->child(i)->index();
    }

    if(idxList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il n'y a pas des element a numéroter"));
        return;
    }

    QModelIndexList idxQueue(idxList);

    int level = itemLevel(idxList.at(0));
    label(&idxList, level);

    do
    {
        QModelIndex idx = idxQueue.takeAt(0);
        QStandardItem *numSubItem = numItemsStdModel->itemFromIndex(idx);
        for(int i=0, n=numSubItem->rowCount(); i<n; i++)
            idxList << numSubItem->child(i)->index();

        if(!idxList.isEmpty())
        {
            idxQueue << idxList;
            int level = itemLevel(idxList.at(0));
            label(&idxList, level);
        }
        else
        {
            QVariant v = numSubItem->data(DataRole::auxptr1());
            if(v.isValid())//generar el vector de coordenadas de la place
            {
                QStringList coorVect;
                QModelIndex seekRoot = numSubItem->index();

                do
                {
                    QString lbl = numItemsStdModel->data(seekRoot, Qt::DisplayRole).toString();
                    coorVect.prepend(lbl);
                    seekRoot = seekRoot.parent();
                }
                while(seekRoot.parent() != QModelIndex());

                Place *place = v.value<Place*>();
                place->setNumbVect(coorVect);
            }
        }
    }
    while(!idxQueue.isEmpty());

    if(!msgLog.isEmpty())
        QMessageBox::information(this, qApp->applicationName(), msgLog.join("\n"));
}

void Site2dView::label(QModelIndexList *idxList, int level)
{
    //clasificar los elementos por la serie a utilizar para numerar
    QList<QModelIndexList> partition;
    while(!idxList->isEmpty())
    {
        QString serie = numItemsStdModel->data(idxList->at(0), DataRole::seriesitenumbitem()).toString();
        QModelIndexList idxSubList;
        for(int i=0; i<idxList->size();)
        {
            if(serie == numItemsStdModel->data(idxList->at(i), DataRole::seriesitenumbitem()).toString())
                idxSubList << idxList->takeAt(i);
            else
                i++;
        }
        partition << idxSubList;
    }

    //Etiquetar
    for(int i=0, n=partition.size(); i<n; i++)
    {
        QModelIndexList idxSubList = partition.at(i);
        QString serie = numItemsStdModel->data(idxSubList.at(0), DataRole::seriesitenumbitem()).toString();

        if(serie.isEmpty())
        {
            QStandardItem *si = schemaItem->child(0,level);
            serie = si->data(DataRole::seriesitenumbitem()).toString();
        }

        SerieManager serMng(serie);
        int N = serMng.getSize();
        if(serMng.isValid())
        {
            if(N>-1 && N<idxSubList.size())
                msgLog << QString("La série \"%1\" a été insuffisante pour numéroter tous les element").arg(serie);
        }
        else
            msgLog << QString("La série \"%1\" n'est pas valide").arg(serie);

        for(int j=0, m=idxSubList.size(); j<m; j++)
        {
            QString lbl = serMng.getNext();
            if(lbl.isEmpty())
                break;
            QVariant v = numItemsStdModel->data(idxSubList.at(j), Qt::DisplayRole);
            if(v.isValid() && !v.toString().isEmpty())//Este elemento prescinde de este nivel de numeración, o sea es parcialmente numerado.
                numItemsStdModel->setData(idxSubList.at(j), lbl, Qt::DisplayRole);
        }
    }
}

void Site2dView::on_tbNewNum_clicked()
{
    QList<QGraphicsItem*> selItems = tixrGraphicsView->scene()->selectedItems();
    if(selItems.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner des places."));
        return;
    }

    QModelIndexList idxList = columnView->selectionModel()->selectedRows();
    if(idxList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner au moins un element de numérotation"));
        return;
    }

    QModelIndex idx = idxList.at(0);
    int level = itemLevel(idx);
    bool makeNew = idxList.size()>1;

    if(level==schemaItem->columnCount()-1 || level==schemaItem->columnCount()-2)
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Impossible modifier les element de ce niveau"));
        return;
    }

    QList<QStandardItem*> itemsList, newItemsList, emptyItemsList;
    for(int i=0, n=selItems.size(); i<n; i++)
    {
        QStandardItem *stdItem = ((Place*)selItems.at(i))->labelItem;
        QModelIndex stdItemIdx = stdItem->index();
        if(!makeNew)
            if(isChild(idx, stdItemIdx))
                continue;

        itemsList << stdItem;
        newItemsList << stdItem;
    }

    if(itemsList.isEmpty())
        return;

    QMap<QStandardItem*, QStandardItem*> oldNewMap;
    for(int k=schemaItem->columnCount()-2; k>level; k--)
    {
        for(int i=0; i<itemsList.size();)
        {
            QStandardItem *stdItem = itemsList.at(i),
                          *oldParent = stdItem->parent(), *newParent;

            if(oldNewMap.contains(oldParent))
            {
                newParent = oldNewMap.value(oldParent);
                itemsList.removeAt(i);
                newItemsList.removeAt(i);
            }
            else
            {
                newParent = new QStandardItem(oldParent->text());
                QVariant v = oldParent->data(DataRole::nomsitenumbitem());
                if(v.isValid())
                    newParent->setData(v, DataRole::nomsitenumbitem());
                v = oldParent->data(DataRole::seriesitenumbitem());
                if(v.isValid())
                    newParent->setData(v, DataRole::seriesitenumbitem());
                v = oldParent->data(DataRole::auxptr1());
                if(v.isValid())
                    newParent->setData(v, DataRole::auxptr1());
                oldNewMap.insert(oldParent, newParent);
                itemsList.replace(i, oldParent);
                newItemsList.replace(i, newParent);
                i++;
            }

            oldParent->takeRow(stdItem->row());
            newParent->appendRow(stdItem);
            if(!oldParent->hasChildren())
                emptyItemsList << oldParent;
        }
        oldNewMap.clear();

        for(int i=0; i<newItemsList.size()-1; i++)
        {
            QStandardItem *stdItem = newItemsList.at(i);
            QString lbl = stdItem->text();
            for(int j=i+1; j<newItemsList.size();)
            {
                QStandardItem *stdItem2 = newItemsList.at(j);
                if(lbl == stdItem2->text())
                {
                    stdItem->appendRows(stdItem2->takeColumn(0));
                    newItemsList.removeAt(j);
                }
                else
                    j++;
            }
        }
    }

    QStandardItem *newItem, *refItem = 0;
    if(makeNew)
    {
        QString lbl = nextTmpLabel(idx, refItem);
        newItem = new QStandardItem(lbl);
        if(refItem)
            newItem->insertRow(refItem->row(), newItemsList);
        else
            newItem->appendRows(newItemsList);
        numItemsStdModel->itemFromIndex(idx.parent())->appendRow(newItem);
    }
    else
    {
        newItem = numItemsStdModel->itemFromIndex(idx);

        for(int j=0, m=newItem->rowCount(); j<m; j++)
        {
            QStandardItem *stdItem2 = newItem->child(j);
            QString lbl2 = stdItem2->text();
            for(int i=0; i<newItemsList.size();)
            {
                QStandardItem *stdItem = newItemsList.at(i);
                if(lbl2 == stdItem->text())
                {
                    stdItem2->appendRows(stdItem->takeColumn(0));
                    newItemsList.removeAt(i);
                    delete stdItem;
                }
                else
                    i++;
            }
        }
        if(!newItemsList.isEmpty())
            newItem->appendRows(newItemsList);
    }

    for(int i=0, n=emptyItemsList.size(); i<n; i++)
    {
        QStandardItem *stdItem = emptyItemsList.at(i);
        QStandardItem *parentItem = stdItem->parent();
        do
        {
            parentItem->removeRow(stdItem->row());
            if(!parentItem->hasChildren())
            {
                stdItem = parentItem;
                parentItem = parentItem->parent();
            }
            else
                break;
        }
        while(parentItem);
    }

    columnView->selectionModel()->clearSelection();
}

void Site2dView::on_tbDelNum_clicked()
{
    QList<QGraphicsItem*> selItems = tixrGraphicsView->scene()->selectedItems();
    QModelIndexList idxList = columnView->selectionModel()->selectedRows();

    if(idxList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner au moins un element de numérotation"));
        return;
    }

    QModelIndex idx = idxList.at(0);
    int level = itemLevel(idx);
    if(level==schemaItem->columnCount()-1 || level==schemaItem->columnCount()-2)
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Impossible modifier les element de ce niveau"));
        return;
    }

    QStandardItem *stdItem = numItemsStdModel->itemFromIndex(idx);
    QStandardItem *parentStdItem = stdItem->parent();

    QStandardItem *oldNullItem = 0;
    for(int i=0, n=parentStdItem->rowCount(); i<n; i++)
    {
        QStandardItem *stdItem2 = parentStdItem->child(i);
        if(stdItem2->text().isEmpty())
        {
            oldNullItem = stdItem2;
            break;
        }
    }
    QStandardItem *nullItem = new QStandardItem;

    if(selItems.isEmpty())
    {
        QList<QStandardItem*> itemsList;
        for(int i=0, n=idxList.size(); i<n; i++)
        {
            idx = idxList.at(i);
            QVariant v = numItemsStdModel->data(idx);
            if(v.isValid() && !v.toString().isEmpty())
                itemsList << numItemsStdModel->itemFromIndex(idx);
        }
        if(oldNullItem)
            itemsList << oldNullItem;

        for(int i=0, n=itemsList.size(); i<n; i++)
        {
            stdItem = itemsList.at(i);
            parentStdItem->takeRow(stdItem->row());
            nullItem->appendRows(stdItem->takeColumn(0));
            delete stdItem;
        }

        parentStdItem->appendRow(nullItem);

        return;
    }

    QList<QStandardItem*> itemsList, newItemsList, emptyItemsList;
    if(oldNullItem)
        idx = oldNullItem->index();
    for(int i=0, n=selItems.size(); i<n; i++)
    {
        QStandardItem *stdItem = ((Place*)selItems.at(i))->labelItem;
        if(!oldNullItem && isChild(idx, stdItem->index()))
            continue;
        itemsList << stdItem;
        newItemsList << stdItem;
    }

    if(itemsList.isEmpty())
        return;

    QMap<QStandardItem*, QStandardItem*> oldNewMap;
    for(int k=schemaItem->columnCount()-2; k>level; k--)
    {
        for(int i=0; i<itemsList.size();)
        {
            QStandardItem *stdItem = itemsList.at(i),
                          *oldParent = stdItem->parent(), *newParent;

            if(oldNewMap.contains(oldParent))
            {
                newParent = oldNewMap.value(oldParent);
                itemsList.removeAt(i);
                newItemsList.removeAt(i);
            }
            else
            {
                newParent = new QStandardItem(oldParent->text());
                QVariant v = oldParent->data(DataRole::nomsitenumbitem());
                if(v.isValid())
                    newParent->setData(v, DataRole::nomsitenumbitem());
                v = oldParent->data(DataRole::seriesitenumbitem());
                if(v.isValid())
                    newParent->setData(v, DataRole::seriesitenumbitem());
                v = oldParent->data(DataRole::auxptr1());
                if(v.isValid())
                    newParent->setData(v, DataRole::auxptr1());
                oldNewMap.insert(oldParent, newParent);
                itemsList.replace(i, oldParent);
                newItemsList.replace(i, newParent);
                i++;
            }

            oldParent->takeRow(stdItem->row());
            newParent->appendRow(stdItem);
            if(!oldParent->hasChildren())
                emptyItemsList << oldParent;
        }
        oldNewMap.clear();

        for(int i=0; i<newItemsList.size()-1; i++)
        {
            QStandardItem *stdItem = newItemsList.at(i);
            QString lbl = stdItem->text();
            for(int j=i+1; j<newItemsList.size();)
            {
                QStandardItem *stdItem2 = newItemsList.at(j);
                if(lbl == stdItem2->text())
                {
                    stdItem->appendRows(stdItem2->takeColumn(0));
                    newItemsList.removeAt(j);
                }
                else
                    j++;
            }
        }
    }

    if(oldNullItem)
    {
        nullItem->appendRows(oldNullItem->takeColumn(0));
        parentStdItem->removeRow(oldNullItem->row());
    }
    nullItem->appendRows(newItemsList);
    parentStdItem->appendRow(nullItem);

    for(int i=0, n=emptyItemsList.size(); i<n; i++)
    {
        QStandardItem *stdItem = emptyItemsList.at(i);
        QStandardItem *parentItem = stdItem->parent();
        do
        {
            parentItem->removeRow(stdItem->row());
            if(!parentItem->hasChildren())
            {
                stdItem = parentItem;
                parentItem = parentItem->parent();
            }
            else
                break;
        }
        while(parentItem);
    }
}

void Site2dView::on_tbNewBloc_clicked()
{
    QList<QGraphicsItem*> selItems = tixrGraphicsView->scene()->selectedItems();
    if(selItems.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner des places."));
        return;
    }

    QModelIndexList idxList = tableView_3->selectionModel()->selectedRows();
    QStandardItem *stdItem, *refItem = 0;
    if(idxList.isEmpty() || idxList.size()>1)
    {
        int bPrior = nextBlocPriority(refItem);
        stdItem = new QStandardItem;
        stdItem->setData(bPrior, Qt::EditRole);
        stdItem->setData(QVariant::fromValue(new QList<Place*>), DataRole::placesinset());

        blocMap.insert(bPrior, stdItem);
    }
    else
        stdItem = blocsStdModel->itemFromIndex(idxList.at(0));
    QList<Place*> *pList = stdItem->data(DataRole::placesinset()).value<QList<Place*>*>();

    QStandardItem *nItem;

    for(int i=0, n=selItems.size(); i<n; i++)
    {
        Place *place = (Place*)selItems.at(i);

        if(place->blocItem!=stdItem)
        {
            if(place->blocItem)
            {
                QList<Place*> *pList2 = place->blocItem->data(DataRole::placesinset()).value<QList<Place*>*>();
                pList2->removeAll(place);
                if(pList2->isEmpty())
                {
                    blocItem->removeRow(place->blocItem->index().row());
                }
                else
                {
                    nItem = blocItem->child(place->blocItem->index().row(), 1);
                    nItem->setData(pList2->size(), Qt::DisplayRole);
                }
            }
            pList->append(place);
            place->blocItem = stdItem;
        }
    }

    nItem = new QStandardItem;//Size's item :-)
    nItem->setData(pList->size(), Qt::DisplayRole);
    nItem->setEditable(false);
    QList<QStandardItem*> iList;
    iList << stdItem;
    iList << nItem;
    nItem = new QStandardItem;//Item pour le sens du premier rang :-)
    nItem->setEditable(false);
    nItem->setCheckable(true);
    nItem->setCheckState(Qt::Checked);
    iList << nItem;
    if(idxList.size()!=1)
    {
        if(refItem)
            blocItem->insertRow(refItem->row(), iList);
        else
            blocItem->appendRow(iList);
    }

    int i=0, n=blocItem->rowCount();
    if(n)
    {
        QList<QColor> colorsList = distinctColors(n);
        for(; i<n; i++)
        {
            stdItem = blocItem->child(i);
            QColor c = colorsList.at(i);
            stdItem->setData(c, Qt::DecorationRole);
            colorList.append(c.name().toLower());
        }
        updateImgCache();
    }

    showVisualInfo(2);
}

void Site2dView::on_pbSetLocalOrder_clicked()
{
    orderOfRangInBloc.insert(blocsStdModel->data(selectedBlocs.first(), Qt::EditRole).toInt(), selectedRows);

    tixrGraphicsView->scene()->blockSignals(true);
    //selectedRowsMap.clear();
    //selectedRows.clear();

    QList<QGraphicsItem*> selItemList = tixrGraphicsView->scene()->selectedItems();
    for(int i=selItemList.size()-1; i>=0; i--)
    {
        selItemList.at(i)->setSelected(false);
        static_cast<Place*>(selItemList.at(i))->setEtat(1);
    }

    QList<Place*> *pPlaceList = blocsStdModel->data(selectedBlocs.first(), 33).value<QList<Place*>*>();
    QList<QString> rangList = orderOfRangInBloc.value( blocsStdModel->data(selectedBlocs.first(), Qt::EditRole).toInt() );

    if(pPlaceList && !pPlaceList->isEmpty() && !rangList.isEmpty())
    {
        QMap<QString, Place*> rowToFirstPlaceInBlocMap;
        //TODO: Basado en el orden relativo de las filas en cada bloque encontrar la primera plaza en cada fila dentro del bloque.
        for(int i=0, n=pPlaceList->size(); i<n; i++)
        {
            Place *place = pPlaceList->at(i);
            if(rowToFirstPlaceInBlocMap.contains(place->row))
            {
                Place *place2 = rowToFirstPlaceInBlocMap.value(place->row);
                if(place->pos.toInt() < place2->pos.toInt())
                    rowToFirstPlaceInBlocMap.insert(place->row, place);
            }
            else
                rowToFirstPlaceInBlocMap.insert(place->row, place);
        }

        for(QMap<QString, Place*>::const_iterator it=rowToFirstPlaceInBlocMap.constBegin(); it!=rowToFirstPlaceInBlocMap.constEnd(); it++)
        {
            int pos = rangList.indexOf(it.key());
            if(pos>=0)
            {
                Place *place = it.value();
                place->rowCoorItem->setText(QString::number(pos));
                place->rowCoorItem->show();
            }
        }
    }

    tixrGraphicsView->scene()->blockSignals(false);
    tixrGraphicsView->scene()->update();
}

void Site2dView::on_pbSetTotalOrder_clicked()
{
    tixrGraphicsView->scene()->blockSignals(true);

    //limpiar el orden de asignación automática de todos los elementos
    for(int i=0, n=placeItemsList.size(); i<n; i++)
    {
        Place *place = placeItemsList.at(i);
        place->selectorder.clear();
        place->coorItem->hide();
    }

    int selectorder = 0;
    QList<int> bIDs = orderOfRangInBloc.keys();
    QList<QStringList> setOfRangList = orderOfRangInBloc.values();
    for(int i=0, n=setOfRangList.size(); i<n; i++)
    {
        QStandardItem *bItem = blocMap.value(bIDs.at(i));
        bool dir = blocItem->child(bItem->row(), 2)->checkState()==Qt::Checked;//Sentido directo(TRUE) o inverso(FALSE) de la primera fila

        QList<QString> rangList = setOfRangList.at(i);
        for(int j=0, m=rangList.size(); j<m; j++)
        {
            QString rID = rangList.at(j);//id de la fila

            QList<Place*> placeList = pathToPlaceMap.value(rID).values();
            for(int k=(dir?0:placeList.size()-1); (dir?k<placeList.size():k>=0); (dir?k++:k--))
            {
                Place *place = placeList.at(k);
                if(place->blocItem==bItem)
                {
                    place->selectorder = QString::number(++selectorder);
                    place->coorItem->setText(place->selectorder);
                    place->coorItem->show();
                }
            }

            dir = !dir;
        }
    }

    tixrGraphicsView->scene()->blockSignals(false);
    tixrGraphicsView->scene()->update();
}

void Site2dView::on_tableView_3_clicked(const QModelIndex & index)
{
    Q_UNUSED(index)
    selectedBlocs = tableView_3->selectionModel()->selectedRows();
    showVisualInfo(2);
}

void Site2dView::on_tbDelBloc_clicked()
{
    QModelIndexList idxList = tableView_3->selectionModel()->selectedRows();
    if(idxList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner au moins un bloc."));
        return;
    }

    QList<QStandardItem*> itemList;
    for(int i=0, n=idxList.size(); i<n; i++)
    {
        QModelIndex idx = idxList.at(i);
        itemList << blocsStdModel->itemFromIndex(idx);
    }

    for(int i=0, n=itemList.size(); i<n; i++)
    {
        QStandardItem *stdItem = itemList.at(i);
        QList<Place*> *pList = stdItem->data(DataRole::placesinset()).value<QList<Place*>*>();
        for(int j=0, m=pList->size(); j<m; j++)
        {
            Place *place = pList->at(j);
            place->blocItem = 0;
        }
        pList->clear();
        blocItem->removeRow(stdItem->index().row());
    }

    int i=0, n=blocItem->rowCount();
    if(n)
    {
        QList<QColor> colorsList = distinctColors(n);
        for(; i<n; i++)
        {
            QStandardItem *stdItem = blocItem->child(i);
            QColor c = colorsList.at(i);
            stdItem->setData(c, Qt::DecorationRole);
            colorList.append(c.name());
        }
        updateImgCache();
    }
}

QList<QColor> Site2dView::distinctColors(int n)
{
    QList<QColor> result;

    qsrand(255);
    for(int i = 0; i<360; i+=360/n)
    {
        result << QColor::fromHsl(i, (90+qrand()*10)%255, (50+qrand()*10)%255);
    }

    return result;
}

/**
  Retorna el primer valor de prioridad que no está siendo utilizado
**/
int Site2dView::nextBlocPriority(QStandardItem *&refItem)
{
    int k = 0, i = 0, n = blocItem->rowCount();
    for(; i<n; i++)
    {
        int m = blocItem->child(i)->data(Qt::EditRole).toInt();
        if(m-k>1)
            break;
        else
            k = m;
    }
    if(i<n)
        refItem = blocItem->child(i);
    return ++k;
}

QString Site2dView::nextTmpLabel(QModelIndex parentIdx, QStandardItem *&refItem)
{
    int level = itemLevel(parentIdx);
    QStandardItem *si = schemaItem->child(0,level);
    QString prefix = si->data(DataRole::nomsitenumbitem()).toString().left(1) + "-";

    QStandardItem *stdItem = numItemsStdModel->itemFromIndex(parentIdx);
    QRegExp rx(prefix + "(\\d+)");
    int k = 0, i = 0, n = stdItem->rowCount();
    for(; i<n; i++)
    {
        if(rx.exactMatch(stdItem->child(i)->text()))
        {
            int m = rx.cap(1).toInt();
            if(m-k>1)
                break;
            else
                k = m;
        }
    }
    QString result = prefix + QString::number(++k);
    if(i<n)
        refItem = blocItem->child(i);
    return result;
}

int Site2dView::itemLevel(QModelIndex index)
{
    int hierarchyLevel=1;
    QModelIndex seekRoot = index;
    while(seekRoot.parent() != QModelIndex())
    {
        seekRoot = seekRoot.parent();
        hierarchyLevel++;
    }
    return hierarchyLevel - 2;
}

QList<QStandardItem*> Site2dView::parentsOfLevel(int level)
{
    QList<QStandardItem*> result;
    result << numItem;

    for(int i=0; i<level; i++)
    {
        for(int j=0, m=result.size(); j<m; j++)
        {
            QStandardItem *stdItem = result.takeAt(0);
            for(int k=0, n=stdItem->rowCount(); k<n; k++)
                result << stdItem->child(k);
        }
    }
    return result;
}

bool Site2dView::isChild(QModelIndex parentIdx, QModelIndex childIdx)
{
    QModelIndex seekRoot = childIdx;
    while(seekRoot.parent() != QModelIndex() && seekRoot.parent() != parentIdx)
    {
        seekRoot = seekRoot.parent();
    }

    return seekRoot.parent() == parentIdx;
}

void Site2dView::on_tbConfigNumLevel_clicked()
{
    QModelIndexList idxList = columnView->selectionModel()->selectedIndexes();
    if(idxList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut selectioné au moins un element"));
        return;
    }
    QModelIndex index = idxList.at(0);
    int level = itemLevel(index);

    QString nom, nom0, serie, serie0;

    QStandardItem *si = schemaItem->child(0,level);
    nom0 = si->data(DataRole::nomsitenumbitem()).toString();
    serie0 = si->data(DataRole::seriesitenumbitem()).toString();

    QVariant v = numItemsStdModel->data(index, DataRole::seriesitenumbitem());
    if(v.isValid())
    {
        nom = numItemsStdModel->data(index, DataRole::nomsitenumbitem()).toString();
        serie = v.toString();
    }
    else
    {
        nom = nom0;
        serie = serie0;
    }

    for(int i=1, n=idxList.size(); i<n; i++)
    {
        QString nom2, serie2;
        QModelIndex index2 = idxList.at(i);
        QVariant v = numItemsStdModel->data(index2, DataRole::seriesitenumbitem());
        if(v.isValid())
        {
            nom2 = numItemsStdModel->data(index2, DataRole::nomsitenumbitem()).toString();
            serie2 = v.toString();
        }
        else
        {
            QStandardItem *si = schemaItem->child(0,level);
            nom2 = si->data(DataRole::nomsitenumbitem()).toString();
            serie2 = si->data(DataRole::seriesitenumbitem()).toString();
        }

        if(nom!=nom2 || serie!=serie2)
        {
            QMessageBox::information(this, qApp->applicationName(), tr("Il y a des paramčtres incompatibles dans la sélection"));
            return;
        }
    }

    SchemaNumDialog schemaNumDialog(this);
    schemaNumDialog.setWindowModality(Qt::WindowModal);
    schemaNumDialog.setNom(nom);
    schemaNumDialog.setSerie(serie);
    schemaNumDialog.setConfMode();

    int result = schemaNumDialog.exec();
    if(result)
    {
        if(schemaNumDialog.useDefaultValues())
        {
            nom = nom0;
            serie = serie0;
        }
        else
        {
            nom = schemaNumDialog.nom();
            serie = schemaNumDialog.serie();
        }

        for(int i=0, n=idxList.size(); i<n; i++)
        {
            index = idxList.at(i);
            numItemsStdModel->setData(index, nom, DataRole::nomsitenumbitem());
            numItemsStdModel->setData(index, serie, DataRole::seriesitenumbitem());
        }
    }
}

void Site2dView::on_tbAddNumLevel_clicked()
{
    QModelIndexList indexList = tableView_2->selectionModel()->selectedColumns();
    if(indexList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner un element de la structure de numerotation."));
        return;
    }

    QModelIndex idx = indexList.at(0);
    if(idx.column()==schemaItem->columnCount()-1)
    {
        QMessageBox::critical(this, qApp->applicationName(), tr("Impossible d'inserer un nouvel element dans cette possition de la structure."));
        return;
    }

    SchemaNumDialog schemaNumDialog(this);
    schemaNumDialog.setWindowModality(Qt::WindowModal);

    int result = schemaNumDialog.exec();
    if(result)
    {
        QString nom = schemaNumDialog.nom();
        QString serie = schemaNumDialog.serie();

        QString desc = QString("Nom: %1\nConstante: %2\nSérie: %3\nValeur initial: %4\nIncrément: %5\nValeurs ŕ sauter: %6");
        SerieManager sm(serie);
        int N = sm.getSize();
        desc = desc.arg(nom, serie, N>-1?QString::number(N):"INF.");

        QStandardItem *schemaElemItem = new QStandardItem(desc);

        schemaElemItem->setData(nom, DataRole::nomsitenumbitem());
        schemaElemItem->setData(serie, DataRole::seriesitenumbitem());

        int level = idx.column();
        schemaItem->insertColumn(level, QList<QStandardItem*>()<<schemaElemItem);

        QStringList sl;
        for(int i=0, n=schemaItem->columnCount(); i<n; i++)
            sl << schemaItem->child(0,i)->data(DataRole::nomsitenumbitem()).toString();
        schemaStdModel->setHorizontalHeaderLabels(sl);

        //adaptar la numeración existente
        QList<QStandardItem*> parentList = parentsOfLevel(level);
        for(int i=0, n=parentList.size(); i<n; i++)
        {
            QStandardItem *stdItem = parentList.at(i);
            QStandardItem *nullItem = new QStandardItem;
            nullItem->appendRows(stdItem->takeColumn(0));
            stdItem->setRowCount(0);
            stdItem->appendRow(nullItem);
        }

        //actualizar el vector de numeración de cada Place
        for(int i=0, n=placeItemsList.size(); i<n; i++)
        {
            Place *place = placeItemsList.at(i);
            QStringList *ptrCoorVect = place->ptrNumbVect();
            ptrCoorVect->insert(level, "");
        }
    }
}

void Site2dView::on_tbDelNumLevel_clicked()
{
    QModelIndexList indexList = tableView_2->selectionModel()->selectedColumns();
    if(indexList.isEmpty())
    {
        QMessageBox::information(this, qApp->applicationName(), tr("Il faut d'abord sélectionner un element de la structure de numerotation."));
        return;
    }

    QModelIndex idx = indexList.at(0);
    if(idx.column()==schemaItem->columnCount()-1 || idx.column()==schemaItem->columnCount()-2)
    {
        QMessageBox::critical(this, qApp->applicationName(), tr("Impossible d'effacer cet element de la structure."));
        return;
    }

    int level = idx.column();
    schemaItem->removeColumn(level);

    QStringList sl;
    for(int i=0, n=schemaItem->columnCount(); i<n; i++)
        sl << schemaItem->child(0,i)->data(DataRole::nomsitenumbitem()).toString();
    schemaStdModel->setHorizontalHeaderLabels(sl);

    //adaptar la numeración existente
    QList<QStandardItem*> parentList = parentsOfLevel(level);
    for(int i=0, n=parentList.size(); i<n; i++)
    {
        QStandardItem *parentItem = parentList.at(i);
        for(int j=0; j<parentItem->rowCount();)
        {
            QStandardItem *stdSubItem = parentItem->takeRow(j).at(0);
            int m = stdSubItem->rowCount();
            parentItem->insertRows(j, stdSubItem->takeColumn(0));
            j += m;
            delete stdSubItem;
        }
    }

    //actualizar el vector de numeración de cada Place
    for(int i=0, n=placeItemsList.size(); i<n; i++)
    {
        Place *place = placeItemsList.at(i);
        QStringList *ptrCoorVect = place->ptrNumbVect();
        ptrCoorVect->removeAt(level);
    }
}

void Site2dView::changeGroupBoxState(bool newState)
{
    if(!newState)
    {
        static_cast<QGroupBox*>(sender())->setChecked(true);
        return;
    }

    QString sn = sender()->objectName();
    int categ;//categoría de la información a mostrar: zone tarif (1), bloc de remplissage automatique(2), numérotation(3)
    if(sn=="gBoxZone")
    {
        disconnect(gBoxBloc, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxBloc->setChecked(!newState);
        connect(gBoxBloc, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        disconnect(gBoxNum, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxNum->setChecked(!newState);
        connect(gBoxNum, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        disconnect(gBoxReservoir, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxReservoir->setChecked(!newState);
        connect(gBoxReservoir, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        categ = 1;
    }else

    if(sn=="gBoxBloc")
    {
        disconnect(gBoxZone, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxZone->setChecked(!newState);
        connect(gBoxZone, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        disconnect(gBoxNum, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxNum->setChecked(!newState);
        connect(gBoxNum, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        disconnect(gBoxReservoir, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxReservoir->setChecked(!newState);
        connect(gBoxReservoir, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        tableView_3->selectionModel()->clearSelection();
        selectedBlocs.clear();

        categ = 2;
    }else

    if(sn=="gBoxNum")
    {
        disconnect(gBoxZone, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxZone->setChecked(!newState);
        connect(gBoxZone, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        disconnect(gBoxBloc, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxBloc->setChecked(!newState);
        connect(gBoxBloc, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        disconnect(gBoxReservoir, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxReservoir->setChecked(!newState);
        connect(gBoxReservoir, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        categ = 3;
    }else

    {
        disconnect(gBoxZone, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxZone->setChecked(!newState);
        connect(gBoxZone, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        disconnect(gBoxBloc, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxBloc->setChecked(!newState);
        connect(gBoxBloc, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        disconnect(gBoxNum, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));
        gBoxNum->setChecked(!newState);
        connect(gBoxNum, SIGNAL(toggled(bool)), this, SLOT(changeGroupBoxState(bool)));

        categ = 4;
    }

    showVisualInfo(categ);
}

void Site2dView::resizeColumnView(int logicalIndex, int oldSize, int newSize)
{
    Q_UNUSED(logicalIndex)
    Q_UNUSED(oldSize)
    Q_UNUSED(newSize)

    QList<int> list;
    QHeaderView *hView = (QHeaderView*)sender();
    for(int i=0, n=hView->count(); i<n; i++)
        list << hView->sectionSize(i);
    columnView->setColumnWidths(list);
}

void Site2dView::on_tbPrint_clicked()
{
    /*QPrinter printer(QPrinter::HighResolution);
    QPrintDialog printDialog(&printer, this);
    if(printDialog.exec() == QDialog::Accepted)
    {

         printer.setPageSize(QPrinter::A4);
         QPainter painter(&printer);

         // print, fitting the viewport contents into a full page
         tixrGraphicsView->render(&painter);
    }*/

    QString fileNameOut = QFileDialog::getSaveFileName(this, tr("Select output file name"), "", tr("Fichiers images (*.png *.bmp *.jpg)"));
    if(fileNameOut.isNull())
        return;

    QImage img(/*tixrGraphicsView->viewport()->size()*/tixrGraphicsView->scene()->width(), tixrGraphicsView->scene()->height(), QImage::Format_ARGB32);
    QPainter painter(&img);
    tixrGraphicsView->scene()->render(&painter);

    img.save(fileNameOut);
}

#ifndef TIXREDITOR
inline int Site2dView::placesInZtar(const QStandardItem *ztarItem)
{
    if (ztarItem == NULL)
        return 0;

    int m = 0;
    for(int i=0, n=lockedPlaces.size(); i<n; i++)
    {
        Place *place = lockedPlaces.at(i);

        if (place == NULL)
            continue;

        QVariant mi = place->zoneItem->data(DataRole::ikey());
        QVariant md = ztarItem->data(DataRole::ikey());

        if (mi == NULL || md == NULL)
            continue;

        if(QString::compare(mi.toString(), md.toString()) == 0)
            m++;
    }
    return m;
}

void Site2dView::setPlaceState(QList<Place*> pList, int newState)
{
    Q_ASSERT(tixrCore);//esta función sólo está disponible para el modo venta

    int m = pList.size();
    QStandardItem *modelItem = new QStandardItem(1, m);
    modelItem->setData(JsonArray, DataRole::jsonType());

    for(int i=0, n=pList.size(); i<n; i++)
    {
        QStandardItem *modelSubItem = new QStandardItem(1,2);
        modelSubItem->setData(JsonObject, DataRole::jsonType());
        modelItem->setChild(0, i, modelSubItem);

        Place *place = pList.at(i);
        modelSubItem->setData(QVariant::fromValue(place), DataRole::auxptr1());

        QStandardItem *modelSubItem2 = new QStandardItem(QString::number(place->dbId));
        modelSubItem2->setData("srvikey", DataRole::jsonProperty());
        modelSubItem2->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 0, modelSubItem2);

        QStandardItem *modelSubItem3 = new QStandardItem(QString::number(newState));
        modelSubItem3->setData("nstate", DataRole::jsonProperty());
        modelSubItem3->setData(JsonSimple, DataRole::jsonType());
        modelSubItem->setChild(0, 1, modelSubItem3);
    }

    tixrCore->setPgFunction("main.setprodstate");
    currentTaskList << tixrCore->sendMsg(modelItem, HTTP_METHOD_ID::POST, "/api", this, "responseSetPlaceState");
}

inline Place *Site2dView::matchPlace(QStandardItem *root, int pId)
{
    for(int i=0, n=root->columnCount(); i<n; i++)
    {
        Place *place = root->child(0,i)->data(DataRole::auxptr1()).value<Place*>();
        if(place->dbId == pId)
            return place;
    }

    return 0;
}

void Site2dView::responseSetPlaceState(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *requestItem = curReq->getRequestItem();
    QStandardItem *responseItem = curReq->getResponseItem();

    QList<Place*> targetPlaces;
    QString log;
    for(int i=0, n=responseItem->rowCount(); i<n; i++)
    {
        QStandardItem *rMsgItem = responseItem->child(i);
        QString msg = rMsgItem->data(DataRole::errmsg()).toString();
        if(msg.isEmpty())
        {
            int id = rMsgItem->data(DataRole::ikey()).toInt();
            targetPlaces << matchPlace(requestItem, id);
        }
        else
            log += msg + "\n";
    }

    int state = requestItem->child(0,0)->child(0,1)->text().toInt();//asumo que el estado es el mismo para todos los asientos

    for(int i=0, n=targetPlaces.size(); i<n; i++)
    {
        Place *p = targetPlaces.at(i);
        p->setEtat(state);
    }

    if(state == 2)
        lockedPlaces.append(targetPlaces); else
    if(state == 1)
    {
        for(int i=0, n=targetPlaces.size(); i<n; i++)
        {
            lockedPlaces.removeOne(targetPlaces.at(i));
        }
    }

    showPricesAndAvailAreas();

    if(!log.isEmpty())
        QMessageBox::information(this, qApp->applicationName(), tr(log.toUtf8().constData()));

    endHandleResponse(curReq);
}

void Site2dView::unlockPlaces()
{
    QList<QGraphicsItem *> selItems = tixrGraphicsView->scene()->selectedItems();
    QList<Place*> placeForUnlock;
    if(selItems.isEmpty())
        placeForUnlock = lockedPlaces;
    else
        for(int i=0, n=selItems.size(); i<n; i++)
        {
            Place *place = static_cast<Place*>( selItems.at(i) );

            if(lockedPlaces.contains(place))
            {
                placeForUnlock << place;
            }
        }

    if(!placeForUnlock.isEmpty())
        setPlaceState(placeForUnlock, 1);
}

inline QStandardItem *findZtarStdItem(QStandardItem *root, QString dbId)
{
    for(int i=0, n=root->rowCount(); i<n; i++)
    {
        QStandardItem *childItem = root->child(i);
        QString ztarId = childItem->data(DataRole::ikey()).toString();
        if(ztarId==dbId)
            return childItem;
    }

    return 0;
}

void Site2dView::showPricesAndAvailAreas()
{
    QSet<QStandardItem*> ztarNewSet;
    for(int i=0, n=lockedPlaces.size(); i<n; i++)
    {
        Place *place = lockedPlaces.at(i);

        if(!place->zoneItem)
            continue;

        QString ztarId= place->zoneItem->data(DataRole::ikey()).toString();

        QStandardItem *ztarItem = findZtarStdItem(catProdItem->child(0), ztarId);
        if(!ztarItem)
            continue;

        ztarNewSet.insert(ztarItem);
    }

    QSet<QStandardItem*> ztarOldSet(ztarSet);
    ztarSet = ztarNewSet;
    ztarNewSet.subtract(ztarOldSet);
    ztarOldSet.subtract(ztarSet);

    //quitar los componentes para las zonas que ya no están en la selección de asientos
    for(QSet<QStandardItem*>::const_iterator i=ztarOldSet.constBegin(), end=ztarOldSet.constEnd(); i!=end; i++)
    {
        QStandardItem *ztarItem = *i;
        deleteSellingWidgets(ztarItem);
    }

    //actualizar el numero de asientos por cada zona de tarif. en los componentes correspondientes
    updateSellingWidgets();

    for(QSet<QStandardItem*>::const_iterator i=ztarNewSet.constBegin(), end=ztarNewSet.constEnd(); i!=end; i++)
    {
        QStandardItem *ztarItem = *i;

        if(ztarItem->hasChildren())//es bien poco probable que aún esté la solicitud, normalmente ya se habrá recibido la respuesta dado todos los pasos que hay que hacer para, desde la UI, llegar hasta aquí
            createSellingWidgets(ztarItem);
        else
            requestGetCategCli(ztarItem->index());
    }
}

inline void Site2dView::updateSellingWidgets()
{
    QLayout *lay = ctlW3->layout();
    for(int i=0, n=lay->count(); i<n; i++)
    {
        QWidget *w = lay->itemAt(i)->widget();
        if(!w)
            continue;
        QString cn = w->metaObject()->className();
        if(cn=="QGroupBox")
        {
            QLayout *lay2 = w->layout();
            Q_ASSERT(lay2->count()==1);
            QWidget *w2 = lay2->itemAt(0)->widget();
            Q_ASSERT(w2);
            QListView *lv = static_cast<QListView*>(w2);
            QStandardItem *ztarItem = tixrCore->stdModel->itemFromIndex(lv->rootIndex().parent());
            int cp = placesInZtar(ztarItem);
            lv->setProperty("tixrNSeat", QVariant(cp));
        }
    }
}

inline void Site2dView::deleteSellingWidgets(QStandardItem *ztarItem)
{
    QLayout *lay = ctlW3->layout();
    for(int i=0, n=lay->count(); i<n; i++)
    {
        QWidget *w = lay->itemAt(i)->widget();
        if(!w)
            continue;
        QString cn = w->metaObject()->className();
        if(cn=="QGroupBox")
        {
            QLayout *lay2 = w->layout();
            Q_ASSERT(lay2->count()==1);
            QWidget *w2 = lay2->itemAt(0)->widget();
            Q_ASSERT(w2);
            QListView *lv = static_cast<QListView*>(w2);
            if(tixrCore->stdModel->itemFromIndex(lv->rootIndex().parent()) == ztarItem)
            {
                lay->removeWidget(w);
                delete w;
            }
        }
    }
}

inline void Site2dView::createSellingWidgets(const QStandardItem *ztarItem)
{
    QString ztarName = ztarItem->text();

    QGroupBox *gb = new QGroupBox(ztarName, ctlW3);
    gb->setFlat(true);
    gb->setCheckable(true);
    gb->setChecked(true);
    QListView *lv = new QListView(gb);
    lv->setEditTriggers(QAbstractItemView::NoEditTriggers);
    lv->setSelectionMode(QAbstractItemView::SingleSelection);
    lv->setViewMode(QListView::IconMode);
    lv->setModel(tixrCore->stdModel);
    lv->setRootIndex(ztarItem->child(0)->index());
    QVBoxLayout *vLay = new QVBoxLayout(gb);
    vLay->addWidget(lv);
    gb->setLayout(vLay);
    ctlW3->layout()->addWidget(gb);
    connect(gb, SIGNAL(toggled(bool)), lv, SLOT(setVisible(bool)));
    lv->setIconSize(QSize(50,30));
    int nPlaces = placesInZtar(ztarItem);
    lv->setProperty("tixrNSeat", QVariant(nPlaces));
    connect(lv, SIGNAL(activated(QModelIndex)), this, SLOT(sellingWidgetActivated(QModelIndex)));
    lv->setMouseTracking(true);
    lv->installEventFilter(this);
}

void Site2dView::requestGetCategCli(const QModelIndex &index)
{
    QStandardItem *modelSubItem = new QStandardItem(1,1);
    modelSubItem->setData("ItemId", DataRole::jsonProperty());
    modelSubItem->setData(JsonObject, DataRole::jsonType());

    tixrCore->stdModel->itemFromIndex(index)->appendRow(modelSubItem);

    QString idZTarStr = tixrCore->stdModel->data(index, DataRole::zapidgricol()).toString();
    QStandardItem *modelSubItem2 = new QStandardItem(idZTarStr);
    modelSubItem2->setData("srvikey", DataRole::jsonProperty());
    modelSubItem2->setData(JsonSimple, DataRole::jsonType());
    modelSubItem->setChild(0, 0, modelSubItem2);

    tixrCore->setPgFunction("main.getallcatcliandprixinztar");
    currentTaskList << tixrCore->sendMsg(modelSubItem, HTTP_METHOD_ID::POST, "/api", this, "responseGetCategCli");
}

void Site2dView::responseGetCategCli(QString respError, PendingRequest *curReq)
{
    if(!beginHandleResponse(respError, curReq))
        return;

    QStandardItem *p = curReq->getResponseItemParent();

    createSellingWidgets(p);//crear la interfaz de ventas

    endHandleResponse(curReq);
}

void Site2dView::sellingWidgetActivated(const QModelIndex & index)
{
    Q_UNUSED(index);
    QListView *lv = static_cast<QListView*>(sender());
    QStandardItem *ztarItem = tixrCore->stdModel->itemFromIndex(lv->rootIndex().parent());

    //enviar una señal al CartView con el listado de asientos a agregar al carrito
    QList<QStandardItem*> lockedItems;
    for(int i=0, k=spCtlW->value(); k>0 && i<lockedPlaces.size();)
    {
        Place *place = lockedPlaces.at(i);

        //comprobar si el asiento está en la zona de tarifa esperada
        QString s1 = place->zoneItem->data(DataRole::ikey()).toString();
        QString s2 = ztarItem->data(DataRole::ikey()).toString();
        if(s1 != s2)
        {
            i++;
            continue;
        }

        QStandardItem *stdItem = new QStandardItem;

        QString numb = "{" + place->numbVect().join(",") +  "}";
        stdItem->setData(numb, Qt::DisplayRole);

        stdItem->setData(place->dbId, DataRole::ikey());

        stdItem->setData(place->id, DataRole::psvgid());

        stdItem->setData(catProdItem->data(DataRole::ikey()), DataRole::pcatprodid());

        stdItem->setData(2, DataRole::petat());//etat

        lockedItems << stdItem;

        lockedPlaces.removeAt(i);
        k--;
    }

    if(!lockedItems.isEmpty())
        emit addToCart(lockedItems);
}
#endif
#endif
