#ifndef ZONEDIALOG_H
#define ZONEDIALOG_H

#include <QtGlobal>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QDialog>
#else
#include <QtWidgets/QDialog>
#endif

namespace Ui {
class ZoneDialog;
}

class ZoneDialog : public QDialog
{
    Q_OBJECT
    Ui::ZoneDialog *ui;

private slots:
    void pickZoneColor();
    void setParam();

public:
    explicit ZoneDialog(QWidget *parent = 0);
    ~ZoneDialog();
    QString nom;
    int cplaces;
    QString couleur;
    bool tzone;
    QString numUnique;
};

#endif // ZONEDIALOG_H

