#include "zoneheaderview.h"
#include "site2dview.h"
#include <QtGui/QDragEnterEvent>
#include <QtGui/QDragMoveEvent>
#include <QtGui/QDropEvent>
#include <QtGui/QStandardItemModel>
#include <QMimeData>

ZoneHeaderView::ZoneHeaderView(Qt::Orientation orientation, QWidget *parent) : QHeaderView(orientation, parent)
{
    setDropIndicatorShown(true);
    setAcceptDrops(true);
    setDragDropMode(QAbstractItemView::DropOnly);
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    setResizeMode(QHeaderView::ResizeToContents);
#else
    setSectionResizeMode(QHeaderView::ResizeToContents);
#endif
}

void ZoneHeaderView::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat("text/plain"))
    {
        QHeaderView *myHeader;
        if((myHeader = qobject_cast<QHeaderView *>(event->source())) && myHeader->orientation() == Qt::Horizontal)
        {
            event->acceptProposedAction();
            return;
        }
    }

    event->ignore();
}

void ZoneHeaderView::dragMoveEvent(QDragMoveEvent *event)
{
    if(event->mimeData()->hasFormat("text/plain"))
    {
        QHeaderView *myHeader;
        if((myHeader = qobject_cast<QHeaderView *>(event->source())) && myHeader->orientation() == Qt::Horizontal)
        {
            event->acceptProposedAction();
            return;
        }
    }

    event->ignore();
}

void ZoneHeaderView::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasFormat("text/plain") && this->acceptDrops())
    {
        QHeaderView *myHeader;
        if((myHeader = qobject_cast<QHeaderView *>(event->source())) && myHeader->orientation() == Qt::Horizontal)
        {
            int k = visualIndex(logicalIndexAt(event->pos()));
            if(k != -1)
            {
                QStandardItemModel *headerModel = static_cast<QStandardItemModel*>(this->model());
                headerModel->disconnect(site2dView);

                QStandardItem *hItem = headerModel->verticalHeaderItem(k);
                QStringList sect = event->mimeData()->text().split("|", QString::SkipEmptyParts);
                hItem->setData(sect.takeFirst(), DataRole::ikey());
                hItem->setData(sect.takeFirst(), DataRole::ztarnom());

                QStandardItem *prixItem = headerModel->item(0)->child(k, 3);
                prixItem->setText(sect.takeFirst());

                event->acceptProposedAction();

#ifndef TIXREDITOR
                connect(headerModel, SIGNAL(itemChanged(QStandardItem*)), site2dView, SLOT(prixItemChecked(QStandardItem*)));
#endif
                return;
            }
        }
    }

    event->ignore();
}
