#ifndef SITE2DVIEW_H
#define SITE2DVIEW_H

#include <QFileSystemWatcher>
#include <QtSvg/QSvgRenderer>
#include <QtGui/QStandardItemModel>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QGroupBox>
#include <QtGui/QProgressBar>
#include <QtGui/QTableView>
#include <QtGui/QColumnView>
#include <QtGui/QToolButton>
#else
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QColumnView>
#include <QtWidgets/QToolButton>
#endif

#include "../TixRealmClientPlugins/TixrGraphicsViewPlugin/tixrgraphicsview.h"
#include "../TixRealmClientPlugins/MapNavigationWindowPlugin/mapnavigationwindow.h"

#include "sitedetails.h"
#include "customgraphicsscene.h"
#include "spinboxdelegate.h"

#ifndef TIXREDITOR
#include "../TixRealmClient/src/core/tixrcore.h"
#include "../TixRealmClient/src/widget.h"
#endif

class Site2dView :
#ifdef TIXREDITOR
    public QWidget
#else
    public TixrWidget
#endif
{
    Q_OBJECT
    //Widgets
    TixrGraphicsView *tixrGraphicsView;
    QTableView *tableView;
    QTableView *tableView_2;
    QTableView *tableView_3;
    QColumnView *columnView;
    QWidget *ctlPannel;
    QWidget *auxPannel;
    QToolButton *resetButton;
    QSlider *zoomSlider;
    QSlider *rotateSlider;
    QToolButton *tbAddNumLevel;
    QToolButton *tbDelNumLevel;
    QGroupBox *gBoxZone;
    QGroupBox *gBoxBloc;
    QGroupBox *gBoxNum;
    QGroupBox *gBoxReservoir;

    bool toSave;
#ifndef QT_NO_FILESYSTEMWATCHER
    QFileSystemWatcher *fileWatcher;
#endif
    QIODevice *ioDev;
    QString eMsg;
    QStandardItemModel *zonesStdModel;
    QStandardItemModel *schemaStdModel;
    QStandardItemModel *numItemsStdModel;
    QStandardItemModel *blocsStdModel;
    QStandardItem *zonesItem;
    QStandardItem *schemaItem;
    QStandardItem *numItem;
    QPersistentModelIndex prevSelZoneIdx;
    QStringList msgLog;
    QList<Place*> placeItemsList;//Lista de todas los asientos del plano
    QList<Place*> lockedPlaces;//Lista de asientos bloqueados por este operador
    QSet<QStandardItem*> ztarSet;//Cjto de elementos de zonas para los asientos en lockedPlaces
    QGraphicsPixmapItem *fondItem;
    QByteArray bgBA;
    int actualRotationAngle;
    qreal totalScaleFactor;
    int totalAngle;
    QProgressBar *progBar;
    QMap<int, QStandardItem*> blocMap;//La llave es la prioridad del bloque y el valor es el item del bloque en el modelo de datos.
    SpinBoxDelegate *blocsDelegate;

    QWidget *siteCtlsContainer;//contenedor del widget de controles del plano de la sala, artilugio para que Qt no lo muestre en la barra de tareas como una ventana independiente.
    QWidget *ctlWidget;
    QWidget *ctlW1;
    QWidget *ctlW2;
    QWidget *ctlW3;
    QPoint dragPosition;
    MapNavigationWindow *mapNavigationWindow;
#ifndef TIXREDITOR
    QSpinBox *spCtlW;
#endif

    bool editionMode;//!<Modo de funcionamiento: edición(TRUE), venta(FALSE)
/**
 * \property isInEditor establece en que aplicación se usa la interfaz de gestión de planos.
 * Determina, junto a editionMode el comportamiento de la interfaz.
 * \a editionMode
 */
    bool isInEditor;//!<Se está trabajando en el editor(TRUE) o en el cliente(FALSE)

    /*!
    * Caché de iconos de los asientos. En la lista cada posición corresponde a uno de los estados posibles: libre, bloqueada, vendidad,...
    * El contenido de cada posición corresponde a las versiones de los iconos primero por nivel de escala y por cada nivel están las versiones por color para ambos sub-estados (normal, seleccionado).
    * La caché es creada es un nivel más alto: en startwindow en el modo edición y en scMainObj en modo venta. Esto permite compartir la caché a nivel de aplicación entre todos los planos abiertos.
    */
    QList< QMap<int, QMap<QString, QPair<QImage, QImage> > > > *stateIconsCache;//0 - Libre; 1 - Bloquée; 2 - Payée
    QList<QString> colorList;//Todos los colores distintos conocidos por el plano de la sala

    int itemLevel(QModelIndex index);
    QMap< QString, QMap<QString,QString> > svgPathMap;
    QStringList svgPathList;//cuando el plano se carga desde la bd, o sea pertenece a una presentación, entonces los path se almacenan en forma de lista. Si no, o sea el plano se carga desde un fichero svg, entonces los path se almacenan en el mapa declarado en la línea de arriba.
    QList<QStandardItem*> parentsOfLevel(int level);
    bool isChild(QModelIndex parentIdx, QModelIndex childIdx);
    void label(QModelIndexList *idxList, int level);
    QString nextTmpLabel(QModelIndex parentIdx, QStandardItem *&refItem);
    int nextBlocPriority(QStandardItem *&refItem);//Retorna el primer valor de prioridad que no está siendo utilizado
    QList<QColor> distinctColors(int n);
    void populateScene();
#ifndef TIXREDITOR
    void populateSceneFromJson();
#endif
    void genScaledPict(QString fileName);
    void showVisualInfo(int categ);//Representa varias propiedades de manera visual(zone tarifs, numérotation, bloc de remplissage automatique)

    //Admin de la caché de imágenes de estados de asientos
    void updateImgCache(int scaleIdx = 0);

#ifndef TIXREDITOR
    inline Place *matchPlace(QStandardItem *root, int pId);
    inline int placesInZtar(const QStandardItem *ztarItem);
    void showPricesAndAvailAreas();
    void requestGetCategCli(const QModelIndex &index);
    inline void createSellingWidgets(const QStandardItem *ztarItem);
    inline void deleteSellingWidgets(QStandardItem *ztarItem);
    inline void updateSellingWidgets();
#endif

private slots:
    void on_resetButton_clicked();
    void setResetButtonEnabled();
    void setupMatrix();
    void changeFile();

    void on_tbZoomIn_clicked();
    void on_tbZoomOut_clicked();
    void on_tbRotateLeft_clicked();
    void on_tbRotateRight_clicked();

    void on_tbAddZone_clicked();
    void on_tbDelZone_clicked();
    void on_tbAddReservoir_clicked();
    void on_tbDelReservoir_clicked();
    void on_tableView_clicked(const QModelIndex & index);

    void on_tableView_2_clicked(const QModelIndex & index);

    void on_pbAddPlaces_clicked();
    void on_pbDelPlaces_clicked();

    void on_tbNumerate_clicked();
    void on_tbNewNum_clicked();
    void on_tbDelNum_clicked();

    void on_tbNewBloc_clicked();
    void on_tbDelBloc_clicked();
    void on_tableView_3_clicked(const QModelIndex & index);
    void on_pbSetLocalOrder_clicked();
    void on_pbSetTotalOrder_clicked();

    void editSchemaElem(const QModelIndex & index);
    void on_tbAddNumLevel_clicked();
    void on_tbDelNumLevel_clicked();
    void on_tbConfigNumLevel_clicked();

    void changeGroupBoxState(bool newState);

    void resizeColumnView(int logicalIndex, int oldSize, int newSize);

    void on_tbPrint_clicked();

    void onPbEditionModeClicked();
    void onTbSellingModeClicked();

#ifndef TIXREDITOR
    void prixItemChecked(QStandardItem * item);

    void responseSetPlaceState(QString respError, PendingRequest *curReq);
    void responseGetCategCli(QString respError, PendingRequest *curReq);

    void sellingWidgetActivated(const QModelIndex & index);
#endif

protected:
#ifndef TIXREDITOR
    bool eventFilter(QObject *obj, QEvent *event);
    void hideEvent(QHideEvent * event);
    void showEvent(QShowEvent * event);
#endif

public:
#ifdef TIXREDITOR
    explicit Site2dView(QProgressBar *pBar, bool editorMode, bool inEditor, QWidget *parent = 0);
#else
    explicit Site2dView(QProgressBar *pBar, bool editorMode, bool inEditor, TixRealmCore *mObj = 0, QWidget *parent = 0);
#endif
    ~Site2dView();
    QString getSiteDialogName();
    QString errorMsg(){return eMsg;}
    void setToSave();
    void setSaved();
    void removeFileWatcher();
    void updateFileWatcher();
    bool save(QString fileName = "");
    void exportAsJson(QJsonObject &jsonObj, QString &error);
    void loadPlan(QIODevice *iod);
    bool viewportEvent(QEvent *event);
    QList<QGraphicsItem*> getSelItems();
    QList< QPair<QString,QString> > getZonesToGrilleMap();//Lista de pares <Nombre de la zone, Id. columna en la Grille>

#ifndef TIXREDITOR
    void setPlaceState(QList<Place*> pList, int newState);
#endif

    //Set the current centerpoint
    void setCenterOfView(const QPointF& centerPoint);

    bool getEditionMode(){return editionMode;}//Modo de funcionamiento: edición(TRUE), venta(FALSE)

    QList<Place*> getPlaceItemsList(){return placeItemsList;}

    void setOnTopWidgetsVisibility(bool show);
    
    void setStateIconsCache(QList< QMap<int, QMap<QString, QPair<QImage, QImage> > > > *iCache){stateIconsCache = iCache;}

    int statesLimit; //total de estados diferentes (Libre, Bloquée, Payée...)
    QImage *getCachedImg(int state, QString color, bool selected = false);

    int scaleIndex;
    int categInfoToShow;//!<1- Zones tarifs; 2 - Blocs remplissage auto; 3 - Numérotation

    QString svgFile;
    QStandardItem *catProdItem;
    QMap<QString, QMap<int, Place*> > pathToPlaceMap;

    QModelIndexList selectedBlocs;//index de los bloques de asignación automática seleccionados. Para el mecanismo de edición desde mousePressEvent en sitedetails.cpp
    QStandardItem *blocItem;
    QMap<QString, Place*> rowToFirstPlaceInBlocMap;//Primeros places items de la fila que pertenece al bloque seleccionado

    QModelIndexList selectedZones;

    QMap<int, QStringList> orderOfRangInBloc;//orden de las filas por cada bloc: la llave del QMap es el ID(prioridad) del bloc y el valor es una lista de ID de los rang
    QStringList selectedRows;//Lista de ID de las filas(rang) actualmente seleccionados, en el orden de selección. Esta lista será el valor de orderOfRangInBloc para el bloc actualmente editado
    QMap<QString, int> selectedRowsMap;//Durante la selección guarda las filas como llave y como valor la cantidad de elementos seleccionados en esta fila. De forma que al llevar a cero se debe eliminar la fila (en fin, el clásico contador de referencias)

public slots:
    void setEditionMode(bool mode);
#ifndef TIXREDITOR
    void unlockPlaces();
#endif

signals:
    void sceneRotated(int angle);
    void addToCart(QList<QStandardItem*>);

friend class Site2dViewTest;
};

Q_DECLARE_METATYPE(QList<Place*>*)

Q_DECLARE_METATYPE(Place*)

typedef QMap<int, Place*> IntPlaceMap;
Q_DECLARE_METATYPE(IntPlaceMap)

Q_DECLARE_METATYPE(Site2dView*)
#endif // SITE2DVIEW_H
