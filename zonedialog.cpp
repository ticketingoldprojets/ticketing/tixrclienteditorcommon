#include "zonedialog.h"
#include "ui_zonedialog.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QColorDialog>
#else
#include <QtWidgets/QColorDialog>
#endif

ZoneDialog::ZoneDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ZoneDialog)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(pickZoneColor()));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(setParam()));
}

ZoneDialog::~ZoneDialog()
{
    delete ui;
}

void ZoneDialog::pickZoneColor()
{
    QColor c = QColorDialog::getColor();
    if(c.isValid())
    {
        ui->pushButton->setStyleSheet(QString("background-color: %1;").arg(c.name()));
    }
}

void ZoneDialog::setParam()
{
    nom = ui->lineEdit->text();
    cplaces = ui->spinBox->value();
    couleur = ui->pushButton->palette().color(QPalette::Window).name();
    tzone = ui->groupBox->isChecked();
    numUnique = ui->lineEdit_2->text();
}

