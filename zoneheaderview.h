#ifndef ZONEHEADERVIEW_H
#define ZONEHEADERVIEW_H

#include <QObject>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QHeaderView>
#else
#include <QtWidgets/QHeaderView>
#endif

class Site2dView;
class QDragEnterEvent;
class QDragMoveEvent;
class QDropEvent;

class ZoneHeaderView : public QHeaderView
{
    Q_OBJECT

public:
    ZoneHeaderView(Qt::Orientation orientation, QWidget *parent = 0);
    void setSite2dView(Site2dView *sv){ site2dView = sv; }

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *event);

private:
    Site2dView *site2dView;
};

#endif // ZONEHEADERVIEW_H
