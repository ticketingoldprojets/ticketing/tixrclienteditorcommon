#ifndef SCHEMANUMDIALOG_H
#define SCHEMANUMDIALOG_H

#include <QtGlobal>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QDialog>
#else
#include <QtWidgets/QDialog>
#endif

namespace Ui {
class SchemaNumDialog;
}

class SerieManager : public QObject
{
    Q_OBJECT

public:
    SerieManager(QString serieStr="", QObject *parent = 0);
    ~SerieManager();
    bool isValid();
    QString getNext();
    int getSize();
    void setSerie(QString serieStr);

private:
    QRegExp rxSerInf;
    QRegExp rxSerFin;
    QRegExp rxElem;
    QString sequence;
    int index;
    bool valid;
    QString current;
    QList<QStringList> seriElemsList;
    void validate();
};

class SchemaNumDialog : public QDialog
{
    Q_OBJECT
    Ui::SchemaNumDialog *ui;

private slots:
    void on_buttonBox_accepted();

public:
    explicit SchemaNumDialog(QWidget *parent = 0);
    ~SchemaNumDialog();
    void setNom(QString nom);
    QString nom();
    void setSerie(QString ser);
    QString serie();
    void setConfMode();
    bool useDefaultValues();
};

#endif // SCHEMANUMDIALOG_H

