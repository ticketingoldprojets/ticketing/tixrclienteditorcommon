#include "schemanumdialog.h"
#include "ui_schemanumdialog.h"

SerieManager::SerieManager(QString serieStr, QObject *parent) : QObject(parent)
{
    rxSerInf.setPattern("\\s*(\\d+|[A-Z]+)\\s*,\\s*(\\d+|[A-Z]+)\\s*\\.\\.\\.\\s*");
    rxSerFin.setPattern("\\s*(\\d+|[A-Z]+)\\s*,\\s*(\\d+|[A-Z]+)\\s*\\.\\.\\.\\s*(\\d+|[A-Z]+)\\s*");
    rxElem.setPattern("\\s*(\\w+)\\s*");
    valid = false;
    if(!serieStr.isEmpty())
    {
        sequence = serieStr;
        validate();
    }
}

SerieManager::~SerieManager()
{
}

void SerieManager::setSerie(QString serieStr)
{
    valid = false;
    sequence = serieStr;
    validate();
}

bool SerieManager::isValid()
{
    return valid;
}

void SerieManager::validate()
{
    QRegExp sep("\\s*;\\s*");
    QStringList seqChunks = sequence.split(sep, QString::SkipEmptyParts);
    for(int i=0, n=seqChunks.size(); i<n; i++)
    {
        QString seriElem = seqChunks.at(i);

        if(rxSerInf.exactMatch(seriElem))
        {
            QString first = rxSerInf.cap(1);
            QString next = rxSerInf.cap(2);
            bool ok1, ok2;
            first.toInt(&ok1);
            next.toInt(&ok2);
            if(i<n-1 || first==next || ok1!=ok2)
                return;

            if(!ok1 && first.size()!=next.size())
                return;

            QStringList strList;
            strList << first;
            strList << next;
            seriElemsList << strList;
        }else

        if(rxSerFin.exactMatch(seriElem))
        {
            QString first = rxSerFin.cap(1);
            QString next = rxSerFin.cap(2);
            QString last = rxSerFin.cap(3);
            bool ok1, ok2, ok3;
            int n1 = first.toInt(&ok1);
            int n2 = next.toInt(&ok2);
            int n = last.toInt(&ok3);
            if(first==next || ok1!=ok2 || ok1!=ok3)
                return;

            if(!ok1 && (first.size()!=next.size() || first.size()!=last.size()))
                return;

            if(ok1 && (n-n2)%(n2-n1))
                return;

            if(!ok1)
                for(int j=0, m=first.size(); j<m; j++)
                {
                    n1 = first.at(j).unicode();
                    n2 = next.at(j).unicode();
                    n = last.at(j).unicode();

                    if((n-n2)%(n2-n1))
                        return;
                }

            QStringList strList;
            strList << first;
            strList << next;
            strList << last;
            seriElemsList << strList;
        }else
            if(!rxElem.exactMatch(seriElem))
                return;
            else
            {
                QStringList strList;
                strList << rxElem.cap(1);
                seriElemsList << strList;
            }
    }
    index = 0;

    valid = true;
}

QString SerieManager::getNext()
{
    if(!valid)
        return "";

    while(index < seriElemsList.size())
    {
        QStringList strList = seriElemsList.at(index);
        if(strList.size()==2)
        {
            QString first = rxSerInf.cap(1);
            QString next = rxSerInf.cap(2);
            if(current.isEmpty())
            {
                current = first;
                return first;
            }
            else
            {
                bool ok;
                int n1 = first.toInt(&ok);
                if(ok)
                {
                    int d = next.toInt() - n1;
                    current = QString::number(current.toInt() + d);
                    return current;
                }
                else
                {
                    QString str;
                    int j=0, m=first.size();
                    for(; j<m; j++)
                    {
                        char n1 = first.at(j).unicode();
                        char d = next.at(j).unicode() - n1;
                        char n = current.at(j).unicode() + d;

                        if(n <= 90)
                            str += QChar(n);
                        else
                        {
                            current.clear();
                            index++;
                            break;
                        }
                    }
                    if(j==m)
                    {
                        current = str;
                        return current;
                    }
                }
            }
        }else

        if(strList.size()==3)
        {
            QString first = rxSerFin.cap(1);
            QString next = rxSerFin.cap(2);
            QString last = rxSerFin.cap(3);

            if(current.isEmpty())
            {
                current = first;
                return first;
            }
            else
            {
                bool ok;
                int n1 = first.toInt(&ok);
                if(ok)
                {
                    int cur = current.toInt() + next.toInt() - n1;
                    if(cur!=last.toInt())
                    {
                        current = QString::number(cur);
                        return current;
                    }
                    else
                    {
                        current.clear();
                        index++;
                    }
                }
                else
                {
                    QString str;
                    int j=0, m=first.size();
                    for(; j<m; j++)
                    {
                        char n1 = first.at(j).unicode();
                        char d = next.at(j).unicode() - n1;
                        char n = current.at(j).unicode() + d;

                        if(n <= last.at(j).unicode())
                            str += QChar(n);
                        else
                        {
                            current.clear();
                            index++;
                            break;
                        }
                    }
                    if(j==m)
                        current = str;
                }
            }
        }else

        {
            if(current.isEmpty())
            {
                current = strList.at(0);
                return current;
            }
            else
            {
                current.clear();
                index++;
            }
        }
    }

    return "";
}

int SerieManager::getSize()
{
    if(!valid)
        return 0;

    int serieSize = 0;
    for(int i=seriElemsList.size()-1; i>=0; i--)
    {
        QStringList strList = seriElemsList.at(i);
        if(strList.size()==2)
        {
            QString first = rxSerInf.cap(1);
            QString next = rxSerInf.cap(2);

            bool ok;
            first.toInt(&ok);
            if(ok)
            {
                return -1;
            }
            else
            {
                int j=0, m=first.size(), max=-1, maxIdx;
                for(; j<m; j++)
                {
                    char n1 = first.at(j).unicode();
                    char d = next.at(j).unicode() - n1;
                    if(d>max)
                    {
                        max = d;
                        maxIdx = j;
                    }
                }
                char n2 = next.at(maxIdx).unicode();
                serieSize += (90 - n2 - max)/max + 3;
            }
        }else

        if(strList.size()==3)
        {
            QString first = rxSerFin.cap(1);
            QString next = rxSerFin.cap(2);
            QString last = rxSerFin.cap(3);

            bool ok;
            int n1 = first.toInt(&ok);
            if(ok)
            {
                int n2 = next.toInt(&ok);
                int n = last.toInt(&ok);

                int d = n2 - n1;
                serieSize += (n - n2 - d)/d;
            }
            else
            {
                int j=0, m=first.size(), min=30;
                for(; j<m; j++)
                {
                    char n1 = first.at(j).unicode();
                    char n2 = first.at(j).unicode();
                    char d = n2 - n1;
                    char n = last.at(j).unicode();
                    int N = (n - n2 - d)/d;
                    if(N<min)
                    {
                        min = N;
                    }
                }
                serieSize += min;
            }
            serieSize += 3;
        }else
            serieSize++;
    }

    return serieSize;
}
//-----------------------------------------------------------------------------------------------------

SchemaNumDialog::SchemaNumDialog(QWidget *parent) : QDialog(parent), ui(new Ui::SchemaNumDialog)
{
    ui->setupUi(this);
}

SchemaNumDialog::~SchemaNumDialog()
{
    delete ui;
}

void SchemaNumDialog::setNom(QString nom)
{
    ui->lineEdit_4->setText(nom.simplified());
}

QString SchemaNumDialog::nom()
{
    return ui->lineEdit_4->text().simplified();
}

void SchemaNumDialog::setSerie(QString ser)
{
    ser = ser.simplified();
    int i=0, n=ui->serieCB->count()-1;
    QRegExp rx("\\s*,\\s*");
    QStringList sl2 = ser.split(rx, QString::SkipEmptyParts);
    for(; i<n; i++)
    {
        QStringList sl1 = ui->serieCB->itemText(i).split(rx, QString::SkipEmptyParts);

        int m = sl1.size();
        if(m!=sl2.size())
            continue;

        int j=0;
        for(; j<m && sl1.at(j)==sl2.at(j); j++);
        if(j==m)
            break;
    }

    if(i<n)
        ui->serieCB->setCurrentIndex(i);
    else
    {
        ui->serieCB->insertItem(n, ser);
        ui->serieCB->setCurrentIndex(n);
    }
}

QString SchemaNumDialog::serie()
{
    return ui->serieCB->currentText();
}

void SchemaNumDialog::setConfMode()
{
    ui->label_6->setEnabled(false);
    ui->lineEdit_4->setEnabled(false);
    ui->checkBox->setEnabled(true);
}

bool SchemaNumDialog::useDefaultValues()
{
    return ui->checkBox->isChecked();
}

void SchemaNumDialog::on_buttonBox_accepted()
{
    accept();
}
