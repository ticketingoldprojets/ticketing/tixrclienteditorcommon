#ifndef CUSTOMGRAPHICSSCENE_H
#define CUSTOMGRAPHICSSCENE_H

#include <QObject>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QGraphicsScene>
#else
#include <QtWidgets/QGraphicsScene>
#endif

class Site2dView;

class CustomGraphicsScene : public QGraphicsScene
{
    Q_OBJECT

public:
    CustomGraphicsScene(QObject *parent = 0);
    void setSite2dView(Site2dView *s2dV){site2dView = s2dV;}

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
     Site2dView *site2dView;
};

#endif // CUSTOMGRAPHICSSCENE_H
