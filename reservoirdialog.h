#ifndef RESERVOIRDIALOG_H
#define RESERVOIRDIALOG_H

#include <QtGlobal>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QDialog>
#else
#include <QtWidgets/QDialog>
#endif

namespace Ui {
class ReservoirDialog;
}

class ReservoirDialog : public QDialog
{
    Q_OBJECT
    Ui::ReservoirDialog *ui;

private slots:
    void pickZoneColor();
    void setParam();

public:
    explicit ReservoirDialog(QWidget *parent = 0);
    ~ReservoirDialog();
    QString nom;
    int cplaces;
    QString couleur;
    bool tzone;
    QString numUnique;
};

#endif // RESERVOIRDIALOG_H
