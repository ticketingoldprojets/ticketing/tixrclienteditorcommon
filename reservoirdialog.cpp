#include "reservoirdialog.h"
#include "ui_reservoirdialog.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QColorDialog>
#else
#include <QtWidgets/QColorDialog>
#endif

ReservoirDialog::ReservoirDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReservoirDialog)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(pickZoneColor()));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(setParam()));
}

ReservoirDialog::~ReservoirDialog()
{
    delete ui;
}

void ReservoirDialog::pickZoneColor()
{
    QColor c = QColorDialog::getColor();
    if(c.isValid())
    {
        ui->pushButton->setStyleSheet(QString("background-color: %1;").arg(c.name()));
    }
}

void ReservoirDialog::setParam()
{
    nom = ui->lineEdit->text();
    cplaces = ui->spinBox->value();
    couleur = ui->pushButton->palette().color(QPalette::Window).name();
    tzone = ui->groupBox->isChecked();
    numUnique = ui->lineEdit_2->text();
}
