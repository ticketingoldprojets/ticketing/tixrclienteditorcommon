SOURCES += \
    ../TixrClientEditorCommon/zonedialog.cpp \
    ../TixrClientEditorCommon/sitedetails.cpp \
    ../TixrClientEditorCommon/site2dview.cpp \
    ../TixrClientEditorCommon/seriedialog.cpp \
    ../TixrClientEditorCommon/schemanumdialog.cpp \
    ../TixrClientEditorCommon/reservoirdialog.cpp \
    ../TixrClientEditorCommon/customgraphicsscene.cpp \
    ../TixrClientEditorCommon/spinboxdelegate.cpp \
    ../TixrClientEditorCommon/zoneheaderview.cpp
	
HEADERS  += \
    ../TixrClientEditorCommon/zonedialog.h \
    ../TixrClientEditorCommon/sitedetails.h \
    ../TixrClientEditorCommon/site2dview.h \
    ../TixrClientEditorCommon/seriedialog.h \
    ../TixrClientEditorCommon/schemanumdialog.h \
    ../TixrClientEditorCommon/reservoirdialog.h \
    ../TixrClientEditorCommon/customgraphicsscene.h \
    ../TixrClientEditorCommon/spinboxdelegate.h \
    ../TixrClientEditorCommon/zoneheaderview.h
	
FORMS    += \
    ../TixrClientEditorCommon/zonedialog.ui \
    ../TixrClientEditorCommon/seriedialog.ui \
    ../TixrClientEditorCommon/schemanumdialog.ui \
    ../TixrClientEditorCommon/reservoirdialog.ui
